import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Constants } from '../../common/constants.enum';

@Injectable({
  providedIn: 'root'
})
export class AdminLoginService {

  constructor(private http: HttpClient) { }

  login(obj: object): Observable<any>  {
    return this.http.post(Constants.API_URL +'v1/auth/sign_in', obj, {})
      .pipe(map((res: any) => res));
  }
  getAllUsers(): Observable<any>  {
    return this.http.get(Constants.API_URL + '', {})
      .pipe(map((res: any) => res));
  }
}
