import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AdminLoginService } from './admin-login.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { HelperService } from '../../common/helper.service';
import * as CryptoJS from 'crypto-js';
import { Constants } from '../../common/constants.enum';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.scss'],
  providers: [AdminLoginService]
})
export class AdminLoginComponent implements OnInit {
  hide = true;
  loginForm: FormGroup;
  constructor(private router: Router, private fb: FormBuilder, private adminService: AdminLoginService,
              private spinner: NgxSpinnerService, private toastr:ToastrService,
              private helper: HelperService) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['',
      [
        Validators.required,
        Validators.pattern('^[A-Za-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
      ]],
      password:['', [Validators.required, Validators.minLength(6)]],
      role:['Admin']
    });
  }

  onSignin() {
    this.spinner.show();
   // const encryptedPswd = CryptoJS.AES.encrypt(this.loginForm.value.password, Constants.CRYPTO_KEY);
    //this.loginForm.value.password = encryptedPswd.toString();
    this.adminService.login(this.loginForm.value).subscribe((data) => {
      this.spinner.hide();
      this.loginForm.reset();
      if (data) {
        if (data) {
          localStorage.setItem("token", data.token);
          const tokenhelper = new JwtHelperService();
          console.log(tokenhelper)
          let tok=localStorage.getItem('token')
          console.log(tok)
          const isExpired = tokenhelper.isTokenExpired(localStorage.getItem('token'));
          console.log(isExpired)
          const isDecode=tokenhelper.decodeToken(localStorage.getItem('token'));
          console.log(isDecode)
          console.log(isDecode.username)
          
          let nami=localStorage.setItem("username",data.name)
          console.log(nami)
          let idbase=localStorage.setItem("userId",isDecode.user_id)
          console.log(idbase)
          this.toastr.success( 'Trippe');
          console.log(this.toastr.success( 'Trippe'));

          //localStorage.setItem("token", data);
//localStorage.setItem('loginId', data.data[0]._id);
         // localStorage.setItem('role', data.data[0].role);
          this.router.navigateByUrl('admindashboard');
        } else {
          this.toastr.info('Please contact admin', 'Trippe');
        }
      } else {
          this.toastr.info(data.message, 'Trippe');
      }
    });
  }
}
