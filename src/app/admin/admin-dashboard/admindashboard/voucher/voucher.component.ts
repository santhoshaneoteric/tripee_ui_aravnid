import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as CryptoJS from 'crypto-js';
import { ViewChild } from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import * as jsPDF from 'jspdf';
//import 'jspdf-autotable';



export interface UserData {
 
  sno:string;
  vouchername:string;
  voucherprice:string;
  createddate:string;
  expirydate:string;

  Status:string;
  
}


@Component({
  selector: 'app-voucher',
  templateUrl: './voucher.component.html',
  styleUrls: ['./voucher.component.scss']
})
export class VoucherComponent implements OnInit {

  displayedColumns: string[] = ['sno','vouchername','voucherprice','createddate','expirydate','Status'];
  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;


  VoucherForm: FormGroup;
  adminStatus;
  
  
  constructor(private router: Router, 
              private formBuilder: FormBuilder, 
              private activeRoute: ActivatedRoute) { }

  
      ngOnInit() {
        this.VoucherForm = this.formBuilder.group({
          vouchername:['',Validators.required],
          voucherprice:['',Validators.required],
          startdate:['',Validators.required],
          enddate:['',Validators.required]

         
         
        
         
        });
    
    }
    createPdf() {
      var doc = new jsPDF();
  
      doc.setFontSize(18);
      doc.text('My PDF Table', 11, 8);
      doc.setFontSize(11);
      doc.setTextColor(100);
  
  
      (doc as any).autoTable({
        head: this.displayedColumns,
        //body: this.data,
        theme: 'plain',
        didDrawCell: data => {
          console.log(data.column.index)
        }
      })
  
      // Open PDF document in new tab
      doc.output('dataurlnewwindow')
  
      // Download PDF document  
      doc.save('table.pdf');
    }
  }
