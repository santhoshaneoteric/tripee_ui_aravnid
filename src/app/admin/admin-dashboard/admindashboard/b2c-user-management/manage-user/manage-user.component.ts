import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
//import { AdminLoginService } from '../../../admin-login/admin-login.service';

export interface UserData {

  name: string;
  phone: string;
  email: string;
  role: string;
  registerData: string;
  Status: string;
}

@Component({
  selector: 'app-manage-user',
  templateUrl: './manage-user.component.html',
  styleUrls: ['./manage-user.component.scss'],
  //providers: [AdminLoginService]
})
export class ManageUserComponent implements OnInit {
  displayedColumns: string[] = ['name', 'email', 'phone', 'role', 'status', 'edit', 'delete'];
  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private router: Router, ) { }
  ngOnInit() {
    localStorage.setItem('editStatus', 'false');
    // this.adminService.getAllUsers().subscribe((data) => {
    //   if (data.success === '1') {
    //     data.data.forEach(obj => {
    //       obj.name = obj.firstName + ' ' + obj.lastName;
    //     });
    //     this.dataSource = new MatTableDataSource(data.data);
    //     this.dataSource.paginator = this.paginator;
    //     this.dataSource.sort = this.sort;
    //   }
    // });
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  onAdminUserEdit(row) {
    this.router.navigate(['admindashboard/editUser', row._id]);
  }
  onAdminUserDelete() {
  }
}
