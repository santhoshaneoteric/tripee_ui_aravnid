import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Constants } from 'src/app/common/constants.enum';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  create(obj: object): Observable<any>  {
    return this.http.post(Constants.API_URL + 'user/create', obj, {})
      .pipe(map((res: any) => res));
  }
  upload(obj, docName, title) {
    return this.http.post(Constants.API_URL + 'upload/' + docName + '/' + title, obj, {})
      .pipe(
        map((res: any) => res)
        );
  }
  getAllUsers(): Observable<any>  {
    if (localStorage.getItem('role') === 'distributor') {
      return this.http.get(Constants.API_URL + 'distributor/user/getAll/' + localStorage.getItem('loginId'), {})
      .pipe(map((res: any) => res));
    } else {
      return this.http.get(Constants.API_URL + 'user/getAll', {})
      .pipe(map((res: any) => res));
    }
  }
  getSingleAgent(id): Observable<any>  {
    return this.http.get(Constants.API_URL + 'user/getInfo/' + id, {})
      .pipe(map((res: any) => res));
  }
  update(id, obj: object): Observable<any>  {
    return this.http.post(Constants.API_URL + 'user/update/' + id, obj, {})
      .pipe(map((res: any) => res));
  }
}