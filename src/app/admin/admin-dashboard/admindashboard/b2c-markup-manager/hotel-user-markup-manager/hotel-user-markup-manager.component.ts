import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import { FormGroup, FormControl,FormBuilder, Validators } from '@angular/forms';



export interface UserData {

  APIName:string;
  
  Markup :string;
  //MarkupProcess:string;
  UpdatedDate:string;
  //Status:string;
  //Actions:string;
}

@Component({
  selector: 'app-hotel-user-markup-manager',
  templateUrl: './hotel-user-markup-manager.component.html',
  styleUrls: ['./hotel-user-markup-manager.component.scss']
})
export class HotelUserMarkupManagerComponent implements OnInit {
  countries=['India','UK','USA'];
  API=['GRN'];
  //Markup=['Fixed','Percent']
  HotelMarkupForm: FormGroup;
  displayedColumns: string[] = ['APIName','Markup','UpdatedDate','delete']
  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private router: Router,private fb: FormBuilder,) {}


  ngOnInit() {
    this.HotelMarkupForm = this.fb.group({
      API: ['', Validators.required],
     
      markup: ['', Validators.required],
     
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  onSubmit(){}
}
