import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotelUserMarkupManagerComponent } from './hotel-user-markup-manager.component';

describe('HotelUserMarkupManagerComponent', () => {
  let component: HotelUserMarkupManagerComponent;
  let fixture: ComponentFixture<HotelUserMarkupManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotelUserMarkupManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelUserMarkupManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
