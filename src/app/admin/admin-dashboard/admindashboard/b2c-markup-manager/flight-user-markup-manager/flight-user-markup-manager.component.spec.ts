import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightUserMarkupManagerComponent } from './flight-user-markup-manager.component';

describe('FlightUserMarkupManagerComponent', () => {
  let component: FlightUserMarkupManagerComponent;
  let fixture: ComponentFixture<FlightUserMarkupManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightUserMarkupManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightUserMarkupManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
