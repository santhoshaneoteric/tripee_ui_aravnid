import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import { Router, ActivatedRoute } from '@angular/router';
//import { RoleService } from '../../role/role.service';
import { FormGroup, FormControl,FormBuilder, Validators } from '@angular/forms';
//import { SignupService } from '../../../../login-signup/signup/signup.service';
import * as CryptoJS from 'crypto-js';
import { Constants } from 'src/app/common/constants.enum';



export interface UserData {

  flightMarkupForm: FormGroup;
  APIName:string;
  
  Markup :string;
  
  UpdatedDate:string;
 
  
}

@Component({
  selector: 'app-flight-user-markup-manager',
  templateUrl: './flight-user-markup-manager.component.html',
  styleUrls: ['./flight-user-markup-manager.component.scss']
})
export class FlightUserMarkupManagerComponent implements OnInit {
 //countries=['India','UK','USA'];
 API=['TripJack','MultiLink'];
 Markup=['Fixed','Percent']
 flightMarkupForm: FormGroup;
  displayedColumns: string[] = ['APIName','Markup','UpdatedDate','delete']
  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private router: Router ,private fb: FormBuilder, ) {}


  ngOnInit() {
    this.flightMarkupForm = this.fb.group({
      API: ['', Validators.required],
     
      markup: ['', Validators.required],
     
    });
  }



  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  onSubmit(){}

}
