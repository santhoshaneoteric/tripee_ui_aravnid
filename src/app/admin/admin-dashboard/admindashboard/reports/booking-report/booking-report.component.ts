import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';



export interface UserData {
  
 
  BookingId:string;
  Name:string;
  CreateDate:string;
  TravelDate:string;
  Voucher:string;
  CustomerId:string;
  NetPrice:number;
  AgentProfit:number;
  Status:string;
  TotalPrice:number;
 
}

@Component({
  selector: 'app-booking-report',
  templateUrl: './booking-report.component.html',
  styleUrls: ['./booking-report.component.scss']
})
export class BookingReportComponent implements OnInit {

  displayedColumns: string[] = ['Name','CreateDate','TravelDate','Status','BookingId','Voucher','CustomerId','AgentProfit','TotalPrice'];
  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private router: Router){ }
  ngOnInit() {
  }

}
