import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
//import { RoleService } from '../../role/role.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
//import { SignupService } from '../../../../login-signup/signup/signup.service';
import * as CryptoJS from 'crypto-js';
import { Constants } from 'src/app/common/constants.enum';


@Component({
  selector: 'app-payment-cancellation',
  templateUrl: './payment-cancellation.component.html',
  styleUrls: ['./payment-cancellation.component.scss']
})
export class PaymentCancellationComponent implements OnInit {

  roles;
  payments = ['Cash deposit', 'Credit', 'NEFT'];
 

  paymentcancellationForm: FormGroup;
  adminStatus;
 
  constructor(private router: Router, 
              private formBuilder: FormBuilder,
              private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.paymentcancellationForm = this.formBuilder.group({
      Partnername:['',Validators.required],
      bookingreference:['',Validators.required],
      payment:['',Validators.required],
      recieptnumber:['',Validators.required],
    });

}
}
