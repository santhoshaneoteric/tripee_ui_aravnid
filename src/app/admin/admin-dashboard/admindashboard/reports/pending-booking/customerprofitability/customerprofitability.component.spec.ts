import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerprofitabilityComponent } from './customerprofitability.component';

describe('CustomerprofitabilityComponent', () => {
  let component: CustomerprofitabilityComponent;
  let fixture: ComponentFixture<CustomerprofitabilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerprofitabilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerprofitabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
