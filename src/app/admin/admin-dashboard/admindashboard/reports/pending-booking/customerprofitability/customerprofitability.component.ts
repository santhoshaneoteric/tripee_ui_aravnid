import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { MatPaginator } from '@angular/material';


export interface UserData {
  name: string;
  email: string;
  // mobile: string;
  role: string;
  debit: string;

}
@Component({
  selector: 'app-customerprofitability',
  templateUrl: './customerprofitability.component.html',
  styleUrls: ['./customerprofitability.component.scss']
})
export class CustomerprofitabilityComponent implements OnInit {

  displayedColumns: string[] = [ 'position','customerid','customername','status','module','netprice','agentprofit','bookingid','createddate','traveldate'];
  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private router: Router) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource([{position: '1',name: 'inno',email: 'xyz@gmail.com',role: '1',currency: '$',tamount:'160',credit:'500',debit:'200',profit:'20',balance:'800'}]);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  createRole() {
    this.router.navigateByUrl('admindashboard/adminuserform');
  }

}