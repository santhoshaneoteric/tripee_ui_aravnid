import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomersalesComponent } from './customersales.component';

describe('CustomersalesComponent', () => {
  let component: CustomersalesComponent;
  let fixture: ComponentFixture<CustomersalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomersalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomersalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
