import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';



export interface UserData {
  Position:number;
  Module:string;
  BookingId:string;
  Name:string;
  CreateDate:string;
  TravelDate:string;
  Voucher:string;
  CustomerId:string;
  NetPrice:number;
  
  Status:string;
 
 
}

@Component({
  selector: 'app-pending-booking',
  templateUrl: './pending-booking.component.html',
  styleUrls: ['./pending-booking.component.scss']
})
export class PendingBookingComponent implements OnInit {

  
  displayedColumns: string[] = ['Position','Name','CreateDate','TravelDate','Status','Module','BookingId','Voucher','CustomerId','NetPrice'];
  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private router: Router){ }
  ngOnInit() {
  }

}
