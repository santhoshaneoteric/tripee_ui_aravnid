import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';



export interface UserData {
  
 
  BookingId:string;
  BookingDate:string;
  
  TransactionType:string;
  NetPrice:number;
  Status:string;
  TotalPrice:number;
  RefundAmount:number;
 
}

@Component({
  selector: 'app-refund-report',
  templateUrl: './refund-report.component.html',
  styleUrls: ['./refund-report.component.scss']
})
export class RefundReportComponent implements OnInit {

  displayedColumns: string[] = ['BookingId','BookingDate','TransactionType','Status','NetPrice','TotalPrice','RefundAmount'];
  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private router: Router){ }
  ngOnInit() {
  }


}
