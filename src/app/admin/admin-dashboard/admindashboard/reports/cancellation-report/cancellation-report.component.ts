import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';



export interface UserData {
  
 
  BookingId:string;
  Name:string;
  BookingDate:string;
  
  CustomerId:string;
  NetPrice:number;
  Status:string;
  TotalPrice:number;
 
}

@Component({
  selector: 'app-cancellation-report',
  templateUrl: './cancellation-report.component.html',
  styleUrls: ['./cancellation-report.component.scss']
})
export class CancellationReportComponent implements OnInit {

  displayedColumns: string[] = ['Name','BookingDate','Status','BookingId','CustomerId','TotalPrice'];
  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private router: Router){ }
  ngOnInit() {
  }

}
