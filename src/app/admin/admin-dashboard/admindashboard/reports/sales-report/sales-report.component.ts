import { Component, OnInit } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { ViewChild } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
//import { RoleService } from '../../role/role.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
//import { SignupService } from '../../../../login-signup/signup/signup.service';
import * as CryptoJS from 'crypto-js';
import { Constants } from 'src/app/common/constants.enum';


export interface UserData {
  Position:number;
  Module:string;
 BookingReference:string;
  CreateDate:string;
  Currency:string;
  Amount:number;

}

@Component({
  selector: 'app-sales-report',
  templateUrl: './sales-report.component.html',
  styleUrls: ['./sales-report.component.scss']
})
export class SalesReportComponent implements OnInit {
  SalesreportForm: FormGroup;
  Bookings=['Flight Bookings','Hotel Bookings','Flight+Hotel Bookings'];
  apis=['Tripmaza','TripJack','GRN',];
  ids=['1234','2345','3456'];

  displayedColumns: string[] = ['Position','Module', 'CreateDate', 'BookingReference','Currency','Amount'];
  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  optionsDAta: string[];
  wise: string;
  options:string;
  constructor(private router: Router,
    private formBuilder: FormBuilder, 
    private activeRoute: ActivatedRoute) { }
ngOnInit() {
   
  this.SalesreportForm = this.formBuilder.group({
    Bookings:['',Validators.required],
    apiwise:['',Validators.required],

    optionsDAta:['',Validators.required],
    
   
  });
     
     
      
}

onwiseChange(event){
  console.log(event.target.value)
  this.optionsDAta = event.target.value == "api" ? this.apis : this.ids;
}
}