import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import { stringify } from '@angular/compiler/src/util';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
//import { RoleService } from '../../role/role.service';
//import { SignupService } from '../../../../login-signup/signup/signup.service';
import * as CryptoJS from 'crypto-js';
import { Constants } from 'src/app/common/constants.enum';
import { B2bMarkupManagerService } from 'src/app/admin/admin-dashboard/admindashboard/b2b-markup-manager/b2b-markup-manager.service';



export interface UserData {
  
API:string;
HotelPNR:string;
Noofrooms:number;
Status:string;
AdminMarkup:string;
TotalPrice:number;
Action:string;

}
@Component({
  selector: 'app-hotel-report',
  templateUrl: './hotel-report.component.html',
  styleUrls: ['./hotel-report.component.scss'],
  providers: [B2bMarkupManagerService]
})
export class HotelReportComponent implements OnInit {
  agentOrDistributorIds = [];
  selectedAgentOrDistributor;
  showAgentOrDistributor;
  selectedId;
  totalMarkups = [];
  b2bHotelReportForm:FormGroup;
  displayedColumns: string[] = ['API','HotelPNR','Noofrooms','Status','AdminMarkup','TotalPrice','Action'];
  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private router: Router,private fb: FormBuilder,private markupService: B2bMarkupManagerService,) { }


  ngOnInit() {
    

    this.b2bHotelReportForm = this.fb.group({
      id: ['', Validators.required],
     // role: ['', Validators.required],
      startdate: ['', Validators.required],
      enddate: ['', Validators.required],
    });
    // this.b2bFlightReportForm.patchValue({
    //   //role : this.markupRole[0],
    //   //markup : this.markup[0]
    // });
    this.markupService.getAllAgentIds().subscribe(data => {
      this.agentOrDistributorIds = data.data;
      this.b2bHotelReportForm.patchValue({
        id : data.data[0].agentNo
      });
    });
    this.selectedAgentOrDistributor = 'Agent';
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
