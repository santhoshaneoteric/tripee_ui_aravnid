import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import { stringify } from '@angular/compiler/src/util';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { B2bMarkupManagerService } from 'src/app/admin/admin-dashboard/admindashboard/b2b-markup-manager/b2b-markup-manager.service';



export interface UserData {

  RefNo: number;
 
  PNR:string;
  Domestic:string;
 
  Destination:string;
  Departure:string;

Action:string;

}



@Component({
  selector: 'app-flight-report',
  templateUrl: './flight-report.component.html',
  styleUrls: ['./flight-report.component.scss'],
  providers: [B2bMarkupManagerService]
})
export class FlightReportComponent implements OnInit {
  b2bFlightReportForm:FormGroup;
  //markupRole = ['Agent', 'Distributor'];
  agentOrDistributorIds = [];
  selectedAgentOrDistributor;
  showAgentOrDistributor;
  selectedId;
  totalMarkups = [];
  displayedColumns: string[] = [
    'RefNo', 'PNR','Domestic','Destination','Departure','Action'
  ];

  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private router: Router,private fb: FormBuilder,private markupService: B2bMarkupManagerService,) { }


  ngOnInit() {
    this.b2bFlightReportForm = this.fb.group({
      id: ['', Validators.required],
     // role: ['', Validators.required],
      startdate: ['', Validators.required],
      enddate: ['', Validators.required],
    });
    // this.b2bFlightReportForm.patchValue({
    //   //role : this.markupRole[0],
    //   //markup : this.markup[0]
    // });
    this.markupService.getAllAgentIds().subscribe(data => {
      this.agentOrDistributorIds = data.data;
      this.b2bFlightReportForm.patchValue({
        id : data.data[0].agentNo
      });
    });
    this.selectedAgentOrDistributor = 'Agent';
    // if (localStorage.getItem('role') === 'admin') {
    //   this.markupService.fetchAgentOrDistributorMarkups().subscribe(data => {
    //     data.data.agentMarkups.forEach(obj => {
    //       obj.markups.forEach(item => {
    //         if (item.markUpType === 'flight') {
    //           this.totalMarkups.push(item);
    //         }
    //       });
    //     });
    //     data.data.distributorMarkups.forEach(obj => {
    //       obj.markups.forEach(item => {
    //         if (item.markUpType === 'flight') {
    //           this.totalMarkups.push(item);
    //         }
    //       });
    //     });
    //     this.dataSource = new MatTableDataSource(this.totalMarkups);
    //     this.dataSource.paginator = this.paginator;
    //     this.dataSource.sort = this.sort;
    //   });
    // } else if (localStorage.getItem('role') === 'distributor') {
    //   this.markupService.fetchAgentOrDistributorMarkups().subscribe(data => {
    //     data.data.distributorMarkups.forEach(obj => {
    //       obj.markups.forEach(item => {
    //         if (item.markUpType === 'flight') {
    //           this.totalMarkups.push(item);
    //         }
    //       });
    //     });
    //     this.dataSource = new MatTableDataSource(this.totalMarkups);
    //     this.dataSource.paginator = this.paginator;
    //     this.dataSource.sort = this.sort;
    //   });
    // } else {
    //   this.markupService.fetchAgentOrDistributorMarkups().subscribe(data => {
    //     data.data.agentMarkups.forEach(obj => {
    //       obj.markups.forEach(item => {
    //         if (item.markUpType === 'flight') {
    //           this.totalMarkups.push(item);
    //         }
    //       });
    //     });
    //     this.dataSource = new MatTableDataSource(this.totalMarkups);
    //     this.dataSource.paginator = this.paginator;
    //     this.dataSource.sort = this.sort;
    //   });
    // }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
