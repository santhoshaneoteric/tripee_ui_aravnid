import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightMarkupManagerComponent } from './flight-markup-manager.component';

describe('FlightMarkupManagerComponent', () => {
  let component: FlightMarkupManagerComponent;
  let fixture: ComponentFixture<FlightMarkupManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightMarkupManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightMarkupManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
