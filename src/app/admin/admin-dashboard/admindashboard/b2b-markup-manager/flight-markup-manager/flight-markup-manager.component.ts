import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { B2bMarkupManagerService } from '../b2b-markup-manager.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

export interface UserData {
  role: string;
  id: string;
  markup: string;
  markuppercentage: string;
  addedBy: string;
}
@Component({
  selector: 'app-flight-markup-manager',
  templateUrl: './flight-markup-manager.component.html',
  styleUrls: ['./flight-markup-manager.component.scss'],
  providers: [B2bMarkupManagerService]
})
export class FlightMarkupManagerComponent implements OnInit {
  flightMarkup: FormGroup;
  markup = ['Fixed', 'Percent'];
  markupRole = ['Agent', 'Distributor'];
  agentOrDistributorIds = [];
  selectedAgentOrDistributor;
  showAgentOrDistributor;
  selectedId;
  totalMarkups = [];
  displayedColumns: string[] = ['id', 'role', 'markup', 'markuppercentage', 'addedBy', 'delete'];
  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private router: Router, private markupService: B2bMarkupManagerService,
              private fb: FormBuilder, private spinner: NgxSpinnerService,
              private toastr: ToastrService) {}


  ngOnInit() {
    this.flightMarkup = this.fb.group({
      id: ['', Validators.required],
      role: ['', Validators.required],
      markup: ['', Validators.required],
      markuppercentage: ['', Validators.required],
    });
    this.flightMarkup.patchValue({
      role : this.markupRole[0],
      markup : this.markup[0]
    });
    this.markupService.getAllAgentIds().subscribe(data => {
      this.agentOrDistributorIds = data.data;
      this.flightMarkup.patchValue({
        id : data.data[0].agentNo
      });
    });
    this.selectedAgentOrDistributor = 'Agent';
    if (localStorage.getItem('role') === 'admin') {
      this.markupService.fetchAgentOrDistributorMarkups().subscribe(data => {
        data.data.agentMarkups.forEach(obj => {
          obj.markups.forEach(item => {
            if (item.markUpType === 'flight') {
              this.totalMarkups.push(item);
            }
          });
        });
        data.data.distributorMarkups.forEach(obj => {
          obj.markups.forEach(item => {
            if (item.markUpType === 'flight') {
              this.totalMarkups.push(item);
            }
          });
        });
        this.dataSource = new MatTableDataSource(this.totalMarkups);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    } else if (localStorage.getItem('role') === 'distributor') {
      this.markupService.fetchAgentOrDistributorMarkups().subscribe(data => {
        data.data.distributorMarkups.forEach(obj => {
          obj.markups.forEach(item => {
            if (item.markUpType === 'flight') {
              this.totalMarkups.push(item);
            }
          });
        });
        this.dataSource = new MatTableDataSource(this.totalMarkups);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    } else {
      this.markupService.fetchAgentOrDistributorMarkups().subscribe(data => {
        data.data.agentMarkups.forEach(obj => {
          obj.markups.forEach(item => {
            if (item.markUpType === 'flight') {
              this.totalMarkups.push(item);
            }
          });
        });
        this.dataSource = new MatTableDataSource(this.totalMarkups);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onFlightMarkupSave() {
    this.flightMarkup.value.markUpType = 'flight';
    this.flightMarkup.value.addedBy = localStorage.getItem('role');
    this.spinner.show();
    if (this.selectedAgentOrDistributor === 'Agent') {
      this.markupService.addFlightMarkup(this.selectedId, this.flightMarkup.value).subscribe(data => {
        this.spinner.hide();
        if (data.success === '1') {
          this.toastr.success(data.message, 'Geturtrip');
        }
      });
    } else {
      this.markupService.addDistributorFlightMarkup(this.selectedId, this.flightMarkup.value).subscribe(data => {
        this.spinner.hide();
        if (data.success === '1') {
          this.toastr.success(data.message, 'Geturtrip');
        }
      });
    }
  }

  onMarkupDelete(row) {

  }

  onRoleChange(e) {
    this.selectedAgentOrDistributor = e.target.value;
    if (e.target.value === 'Agent') {
      this.showAgentOrDistributor = false;
      this.markupService.getAllAgentIds().subscribe(data => {
        this.agentOrDistributorIds = data.data;
        this.flightMarkup.patchValue({
          id : data.data[0].agentNo
        });
      });
    } else {
      this.showAgentOrDistributor = true;
      this.markupService.getAllDistributorIds().subscribe(data => {
        this.agentOrDistributorIds = data.data;
        this.flightMarkup.patchValue({
          id : data.data[0].distributorNo
        });
      });
    }
  }

  onAgentOrDistributorSelected(e) {
    if (this.selectedAgentOrDistributor === 'Agent') {
      this.agentOrDistributorIds.forEach(obj => {
        if (obj.agentNo === e) {
          this.selectedId = obj._id;
        }
      });
    } else {
      this.agentOrDistributorIds.forEach(obj => {
        if (obj.distributorNo === e) {
          this.selectedId = obj._id;
        }
      });
    }
  }

}
