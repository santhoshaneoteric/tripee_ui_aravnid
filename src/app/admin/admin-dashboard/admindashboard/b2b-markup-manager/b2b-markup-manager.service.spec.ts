import { TestBed } from '@angular/core/testing';

import { B2bMarkupManagerService } from './b2b-markup-manager.service';

describe('B2bMarkupManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: B2bMarkupManagerService = TestBed.get(B2bMarkupManagerService);
    expect(service).toBeTruthy();
  });
});
