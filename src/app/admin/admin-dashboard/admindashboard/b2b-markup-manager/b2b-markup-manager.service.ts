import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Constants } from 'src/app/common/constants.enum';

@Injectable({
  providedIn: 'root'
})
export class B2bMarkupManagerService {

  constructor(private http: HttpClient) { }

  getAllAgentIds(): Observable<any>  {
    return this.http.get(Constants.API_URL + 'agent/getIds', {})
      .pipe(map((res: any) => res));
  }
  getAllDistributorIds(): Observable<any>  {
    return this.http.get(Constants.API_URL + 'distributor/getIds', {})
      .pipe(map((res: any) => res));
  }
  upload(obj, docName, title) {
    return this.http.post(Constants.API_URL + 'upload/' + docName + '/' + title, obj, {})
      .pipe(
        map((res: any) => res)
        );
  }
  getAllAgents(): Observable<any>  {
    if (localStorage.getItem('role') === 'distributor') {
      return this.http.get(Constants.API_URL + 'distributor/agents/getAll/' + localStorage.getItem('loginId'), {})
      .pipe(map((res: any) => res));
    } else {
      return this.http.get(Constants.API_URL + 'agent/getAll', {})
      .pipe(map((res: any) => res));
    }
  }
  getSingleAgent(id): Observable<any>  {
    return this.http.get(Constants.API_URL + 'agent/getInfo/' + id, {})
      .pipe(map((res: any) => res));
  }
  update(id, obj: object): Observable<any>  {
    return this.http.post(Constants.API_URL + 'agent/update/' + id, obj, {})
      .pipe(map((res: any) => res));
  }
  addHotelMarkup(id, obj: object): Observable<any>  {
    return this.http.post(Constants.API_URL + 'agent/hotelMarkup/' + id, obj, {})
      .pipe(map((res: any) => res));
  }
  addDistributorHotelMarkup(id, obj: object): Observable<any>  {
    return this.http.post(Constants.API_URL + 'distributor/hotelMarkup/' + id, obj, {})
      .pipe(map((res: any) => res));
  }
  addFlightMarkup(id, obj: object): Observable<any>  {
    return this.http.post(Constants.API_URL + 'agent/flightMarkup/' + id, obj, {})
      .pipe(map((res: any) => res));
  }
  addDistributorFlightMarkup(id, obj: object): Observable<any>  {
    return this.http.post(Constants.API_URL + 'distributor/flightMarkup/' + id, obj, {})
      .pipe(map((res: any) => res));
  }
  fetchAgentOrDistributorMarkups(): Observable<any>  {
    return this.http.get(Constants.API_URL + 'agent/fetchAgentOrDistributorMarkups', {})
      .pipe(map((res: any) => res));
  }
}

