import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotelMarkupManagerComponent } from './hotel-markup-manager.component';

describe('HotelMarkupManagerComponent', () => {
  let component: HotelMarkupManagerComponent;
  let fixture: ComponentFixture<HotelMarkupManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotelMarkupManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelMarkupManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
