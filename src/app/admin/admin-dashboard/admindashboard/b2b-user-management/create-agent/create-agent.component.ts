import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import * as CryptoJS from 'crypto-js';
import { Constants } from 'src/app/common/constants.enum';
import { NgxSpinnerService } from 'ngx-spinner';
//mport { ToastrService } from 'ngx-toastr';
import { AgentserviceService} from 'src/app/agentsignup/agentservice.service';
@Component({
  selector: 'app-create-agent',
  templateUrl: './create-agent.component.html',
  styleUrls: ['./create-agent.component.scss'],
  providers: [AgentserviceService]
})
export class CreateAgentComponent implements OnInit {
  AgentForm:FormGroup;
  submitBtnFlag;
  states=[];

	cities=[];


 // countries = ["India","USA","UK"];
 // cities=["Hyderabad","Banglore","Mumbai","Chennai"];
  //states=["Andhra Pradesh","Telangana","Karnataka","Tamilnadu"];

  constructor(private router: Router,private formBuilder: FormBuilder,private agentserviceService: AgentserviceService,
    private spinner: NgxSpinnerService,private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.AgentForm=this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [
        Validators.required,
        Validators.pattern('^[A-Za-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
      ]],
      password:['', [Validators.required, Validators.minLength(6)]],
      phone: ['', [Validators.required, Validators.maxLength(10)]],

      companyLocation: ['', Validators.required],
      officeNo: ['',],
      companyWebsite: ['',],
      companypancard: ['', ],
      gstno: ['',],
     // uploadcertificate:['',Validators.required],
     // ownername: [''],
     // ownerAadhar: [''],
     // ownerpancard: [''],
      //ownerphone: [''],
      //owneremail: [''],
    

      country: ['',Validators.required],
      state: ['',Validators.required],

      city: ['',Validators.required],
      zipCode: ['', Validators.required],
      //agentNo: ['',Validators.required],
      agentOrCompanyName: ['', Validators.required],
      role:['Agent']

//pancard: ['',Validators.required],
     // Aadhar: ['',Validators.required],

    });
    

  }
  selectedCountry: String = "";
  
	Countries: Array<any> = [
		{ name: 'Germany', states: [ {name: 'Baveria', cities: ['Duesseldorf', 'Leinfelden-Echterdingen', 'Eschborn']} ] },
		{ name: 'Spain', states: [ {name: 'Barcelona', cities: ['Barcelona']} ] },
		{ name: 'USA', states: [ {name: 'California', cities: ['Downers Grove']} ] },
    { name: 'Mexico', states: [ {name: 'mexicostate', cities: ['Puebla']} ] },
    { name: 'India', states: [ {name: 'Telangana', cities: ['Hyderabad','secunderabad']},
    {name: 'Andhrapradesh', cities: ['vizag','Guntur']},
  
  ] },
		
	];
  
	
	
	changeCountry(country) {
    this.Countries.forEach(cntry => {
      if(cntry.name === country) {
        this.states = cntry.states;
      }
    });
  
	}

	changeState(state) {
    this.states.forEach(sta => {
      if(sta.name === state) {
        this.cities = sta.cities;
      }
    });
		// this.cities = this.Countries.find(cntry => cntry.name == this.selectedCountry).states.find(stat => stat.name == state).cities;
	}
  // changeCity(city) {
  //   this.cities.forEach(cta => {
  //     if(cta.name === city) {
  //       this.cities = cta.cities;
  //     }
  //   });
	// 	// this.cities = this.Countries.find(cntry => cntry.name == this.selectedCountry).states.find(stat => stat.name == state).cities;
	// }




  onCreateAgent() {
    this.spinner.show();
    //this.AgentForm.value.status = 'active';
    this.agentserviceService.create(this.AgentForm.value).subscribe(data => {
      this.spinner.hide();
      if (data) {
        //this.toastr.success(data.message, 'Geturtrip');
        this.router.navigateByUrl('agentaccount');
      }
    });
    // (localStorage.getItem('editStatus') === 'true') 
    // console.log(this.activeRoute.snapshot.params.id)
    this.agentserviceService.getSingleAgent(this.activeRoute.snapshot.params.id).subscribe(data => {
      if (data) {
        console.log(data)
        this.AgentForm.patchValue({
          //console.log(data[0].email),
          email: data.data.email,
          
          agentNo: data[0].id,
          agentOrCompanyName: data[0].agent[0].agentOrCompanyName,
          
          companyWebsite: data[0].agent[0].companyWebsite,
          name:data[0].name,
         // companyLocation: data.data.companyLocation,
          contactNo: data[0].phone,
          officeNo: data[0].officeNo,
          //faxNo: data.data.faxNo,
          zipCode: data[0].zipCode,
          city: data[0].city,
          state: data[0].state,
          country: data[0].country,
          taxNo: data[0].gstno,
          //panNo: data[0].panNo
        });
        
      }
    });
  }






}
