import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { AgentService } from './agent.service';

export interface UserData {
  agentNo: string;
  name: string;
  email: string;
  contactNo: number;
  agencyname: string;
  //createdBy: string;
}
@Component({
  selector: 'app-manage-agent',
  templateUrl: './manage-agent.component.html',
  styleUrls: ['./manage-agent.component.scss'],
  providers: [AgentService]
})
export class ManageAgentComponent implements OnInit {
  displayedColumns: string[] = ['agentNo', 'name', 'agencyname','email', 'contactNo',   'edit', 'delete'];
  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private router: Router, private agentService: AgentService) { }
  ngOnInit() {
   localStorage.setItem('editStatus', 'false');
    this.agentService.getAllAgents().subscribe((data) => {
      if (data[0]) {
        console.log(data[0]);
        console.log(data.agent);
       // console.log(data.agent)
        data.forEach(obj => {
          obj.name=data[0].name
          obj.agencyname = data[0].agent[0].agentOrCompanyName;
          obj.email=data[0].email;
          obj.agentNo=data[0].id;
          obj.contactNo=data[0].phone;
          
        

        });
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  onAgentEdit(row) {
    localStorage.setItem('editStatus', 'true');
    this.router.navigate(['admindashboard/create-agent', row.id]);
  }
  onAgentDelete(row) {

  }
}
