import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Constants } from 'src/app/common/constants.enum';

@Injectable({
  providedIn: 'root'
})
export class AgentService {

  constructor(private http: HttpClient) { }

  create(obj: object): Observable<any>  {
    return this.http.post(Constants.API_URL + 'agent/create', obj, {})
      .pipe(map((res: any) => res));
  }
  upload(obj, docName, title) {
    return this.http.post(Constants.API_URL + 'upload/' + docName + '/' + title, obj, {})
      .pipe(
        map((res: any) => res)
        );
  }
  uploadTicket(obj, docName, title) {
    return this.http.post(Constants.API_URL + 'ticketUpload/' + docName + '/' + title, obj, {})
      .pipe(
        map((res: any) => res)
        );
  }
  getAllAgents(): Observable<any>  {
   
      return this.http.get(Constants.API_URL + 'v1/auth/all_agents', {})
      .pipe(map((res: any) => res));
    
  }
  getSingleAgent(id): Observable<any>  {
    return this.http.get(Constants.API_URL + 'agent/getInfo/' + id, {})
      .pipe(map((res: any) => res));
  }
  update(id, obj: object): Observable<any>  {
    return this.http.post(Constants.API_URL + 'agent/update/' + id, obj, {})
      .pipe(map((res: any) => res));
  }
  saveFlightTicket(obj: object): Observable<any>  {
    return this.http.post(Constants.API_URL + 'upload_flight_ticket', obj, {})
      .pipe(map((res: any) => res));
  }
  getAllFlightTickets(): Observable<any>  {
    return this.http.get(Constants.API_URL + 'getAllFlightTickets', {})
      .pipe(map((res: any) => res));
  }
}

