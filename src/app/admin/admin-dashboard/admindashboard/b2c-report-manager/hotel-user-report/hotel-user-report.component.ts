import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import { stringify } from '@angular/compiler/src/util';
import { FormGroup, FormControl,FormBuilder, Validators } from '@angular/forms';

//import { RoleService } from '../../role/role.service';
//import { SignupService } from '../../../../login-signup/signup/signup.service';
import { Constants } from 'src/app/common/constants.enum';



export interface UserData {

API:string;
HotelPNR:string;
Noofrooms:number;
Status:string;
AdminMarkup:string;
TotalPrice:number;


}

@Component({
  selector: 'app-hotel-user-report',
  templateUrl: './hotel-user-report.component.html',
  styleUrls: ['./hotel-user-report.component.scss'],
 // providers: [RoleService, SignupService],
})
export class HotelUserReportComponent implements OnInit {
  API=['GRN'];
  displayedColumns: string[] = ['API','HotelPNR',
   'Noofrooms','Status','AdminMarkup','TotalPrice','delete'];
   B2CHotelReport:FormGroup
  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private router: Router,private fb: FormBuilder) { }


  ngOnInit() {
    this.B2CHotelReport = this.fb.group({

      api: ['', Validators.required],
      startdate: ['', Validators.required],
      enddate: ['', Validators.required],

    
    });

  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

onSubmit(){
  // alert(JSON.stringify(this.form.value));
}


}
