import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotelUserReportComponent } from './hotel-user-report.component';

describe('HotelUserReportComponent', () => {
  let component: HotelUserReportComponent;
  let fixture: ComponentFixture<HotelUserReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotelUserReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelUserReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
