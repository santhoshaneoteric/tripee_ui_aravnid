import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightUserReportComponent } from './flight-user-report.component';

describe('FlightUserReportComponent', () => {
  let component: FlightUserReportComponent;
  let fixture: ComponentFixture<FlightUserReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightUserReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightUserReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
