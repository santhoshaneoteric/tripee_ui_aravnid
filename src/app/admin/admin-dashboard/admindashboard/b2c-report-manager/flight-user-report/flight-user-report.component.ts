import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import { stringify } from '@angular/compiler/src/util';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';




export interface UserData {

  RefNo: number;

  PNR:string;
  Domestic:string;

  Destination:string;
  Departure:string;






}


@Component({
  selector: 'app-flight-user-report',
  templateUrl: './flight-user-report.component.html',
  styleUrls: ['./flight-user-report.component.scss']
})
export class FlightUserReportComponent implements OnInit {
  API=['Trip Jack','Multi Link'];
  B2CFlightReport:FormGroup;
  displayedColumns: string[] = [
    'RefNo', 'PNR','Domestic','Destination','Departure','delete'
  ];
 

  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private router: Router,private fb: FormBuilder,) { }

  ngOnInit() {
    this.B2CFlightReport = this.fb.group({

      api: ['', Validators.required],
      startdate: ['', Validators.required],
      enddate: ['', Validators.required],

    
    });
}
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  onSubmit(){
    // alert(JSON.stringify(this.form.value));
  }


}
