import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, EmailValidator } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import {VerifyserviceService} from 'src/app/verifyuseraccount/verifyservice.service'
//import { ToastrService } from 'ngx-toastr';
//import { ToastrService } from 'ngx-toastr';
import {ResetService} from 'src/app/resetpassword/reset.service'
import { MatDialog, MatDialogRef  } from '@angular/material/dialog';
import { LoginComponent } from 'src/app/login/login.component';
import {    VerifyuseraccountComponent} from 'src/app/verifyuseraccount/verifyuseraccount.component'
@Component({
  selector: 'app-phoneotp',
  templateUrl: './phoneotp.component.html',
  styleUrls: ['./phoneotp.component.scss'],
  providers: [VerifyserviceService]
})
export class PhoneotpComponent implements OnInit {

  otpfrom:FormGroup;
  obj:any;

  constructor(private router: Router,private formBuilder: FormBuilder,private dialog: MatDialog,
    private dialogRef: MatDialogRef<PhoneotpComponent>,
    private spinner: NgxSpinnerService,private activeRoute: ActivatedRoute,
    private verifyservice:VerifyserviceService) { }


  ngOnInit() {
    
    this.otpfrom=this.formBuilder.group({
      // email: ['', [
      //   Validators.required,
      //   Validators.pattern('^[A-Za-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
      // ]],
      
     otp: ['',Validators.required]
     
    });
    
    
    
      }
      // openSignupDialog() {
      //   this.dialog.open(SignupComponent, {
      //     disableClose: true,
      //     panelClass: 'login-container'
      //   });
      // }
    
      emailDialogClose() {
        this.dialogRef.close();
      }
    
      
     // console.log(otp_value)
      //let obj= {email:email,otp:otp_value}
      // onVerify() {
      //   console.log(this.OtpForm.value);
      //   this.verifyservice.verify(this.OtpForm.value).subscribe(data => {
          
         
      //   });
      // }
      onVerify() {
        this.spinner.show();
      //  localStorage.setItem("email",this.OtpForm.value.email)
    
        // //this.AgentForm.value.status = 'active';
        // let obj={"email":email,"otp":this.OtpForm.value.otp}
        //  console.log(obj)
        console.log(this.otpfrom.value)
        this.verifyservice.email_for_activation(this.otpfrom.value).subscribe(data => {
          this.spinner.hide();
          if (data) {
            console.log(data)
            this.dialogRef.close();
            this.dialog.open(VerifyuseraccountComponent, {
              disableClose: true,
              panelClass: 'login-container'
            });

            //this.toastr.success(data.message, 'Trippe');
            
          }
        });
      }

}
