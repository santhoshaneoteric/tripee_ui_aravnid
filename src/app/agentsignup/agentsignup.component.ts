import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import * as CryptoJS from 'crypto-js';
import { Constants } from 'src/app/common/constants.enum';
import { NgxSpinnerService } from 'ngx-spinner';
//mport { ToastrService } from 'ngx-toastr';
import { AgentserviceService} from 'src/app/agentsignup/agentservice.service';

@Component({
  selector: 'app-agentsignup',
  templateUrl: './agentsignup.component.html',
  styleUrls: ['./agentsignup.component.scss'],
  providers: [ AgentserviceService]

})
export class AgentsignupComponent implements OnInit {
  AgentForm:FormGroup;
  submitBtnFlag;
  states=[];

	cities=[];


 // countries = ["India","USA","UK"];
 // cities=["Hyderabad","Banglore","Mumbai","Chennai"];
  //states=["Andhra Pradesh","Telangana","Karnataka","Tamilnadu"];

  constructor(private router: Router,private formBuilder: FormBuilder,private agentserviceService: AgentserviceService,
    private spinner: NgxSpinnerService,private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.AgentForm=this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [
        Validators.required,
        Validators.pattern('^[A-Za-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
      ]],
      password:['', [Validators.required, Validators.minLength(6)]],
      phone: ['', [Validators.required, Validators.maxLength(10)]],

      companyLocation: ['', Validators.required],
      officeNo: ['',],
      companyWebsite: ['',],
      companypancard: ['', ],
      gstno: ['',],
     // uploadcertificate:['',Validators.required],
     // ownername: [''],
     // ownerAadhar: [''],
     // ownerpancard: [''],
      //ownerphone: [''],
      //owneremail: [''],
    

      country: ['',Validators.required],
      state: ['',Validators.required],

      city: ['',Validators.required],
      zipCode: ['', Validators.required],
      //agentNo: ['',Validators.required],
      agentOrCompanyName: ['', Validators.required],
      role:['Agent']

//pancard: ['',Validators.required],
     // Aadhar: ['',Validators.required],

    });
    
    // this.createAgentForm.patchValue({
    //   currency: this.countries[0]
    // });
    // this.createAgentForm.patchValue({
    //   currency: this.states[0]
    // });
    // this.createAgentForm.patchValue({
    //   currency: this.cities[0]
    // });



  }

  selectedCountry: String = "";
  
	Countries: Array<any> = [
		{ name: 'Germany', states: [ {name: 'Baveria', cities: ['Duesseldorf', 'Leinfelden-Echterdingen', 'Eschborn']} ] },
		{ name: 'Spain', states: [ {name: 'Barcelona', cities: ['Barcelona']} ] },
		{ name: 'USA', states: [ {name: 'California', cities: ['Downers Grove']} ] },
		{ name: 'Mexico', states: [ {name: 'mexicostate', cities: ['Puebla']} ] },
		{ name: 'India', states: [ {name: ['AndhraPradesh'], cities: ['Vijaywada', 'Vizag', 'Kurnool', 'Chittoor','Tirupathy']},{name:['Telangana'],cities:['Hyderabad','Nalgonda','Karimnagar']} ] },
	];
  
	
	
	changeCountry(country) {
    this.Countries.forEach(cntry => {
      if(cntry.name === country) {
        this.states = cntry.states;
      }
    });
  
	}

	changeState(state) {
    this.states.forEach(sta => {
      if(sta.name === state) {
        this.cities = sta.cities;
      }
    });
		// this.cities = this.Countries.find(cntry => cntry.name == this.selectedCountry).states.find(stat => stat.name == state).cities;
	}
 




  onCreateAgent() {
    this.spinner.show();
    //this.AgentForm.value.status = 'active';
    this.agentserviceService.create(this.AgentForm.value).subscribe(data => {
      this.spinner.hide();
      if (data) {
        //this.toastr.success(data.message, 'Geturtrip');
        this.router.navigateByUrl('agentaccount');
      }
    });
  }





















  
  // onAgentSave() {
  //   this.spinner.show();

  //   const encryptedPswd = this.encrypt(this.createAgentForm.value.password);
  //   this.createAgentForm.value.password = encryptedPswd;
  //   this.createAgentForm.value.status = 'active';
  //   this.createAgentForm.value.createdBy = 'Agent';

  //   console.log(this.createAgentForm.value);
  //   this.agentserviceService.create(this.createAgentForm.value).subscribe(data => {
  //     this.spinner.hide();

  //     if (data.success === '1') {
  //       this.toastr.success(data.message, 'Geturtrip');

  //       this.router.navigateByUrl('agentaccount');
  //     }
  //   });
  // }
  



  // onCreateAgent() {
  //   this.spinner.show();
  //   this.createAgentForm.value.companyLogo = this.url;
  //   this.createAgentForm.value.adminOrDistributorId = localStorage.getItem('loginId');
  //   if (localStorage.getItem('role') === 'distributor') {
  //     this.createAgentForm.value.createdBy = 'distributor';
  //   } else {
  //     this.createAgentForm.value.createdBy = 'admin';
  //   }
  //   this.agentService.create(this.createAgentForm.value).subscribe(data => {
  //     this.spinner.hide();
  //     if (data.success === '1') {
  //       this.toastr.success(data.message, 'Geturtrip');
  //       this.router.navigateByUrl('admindashboard/manageAgent');
  //     }
  //   });
  // }
















 
  onAdminUserCancel() {
    this.router.navigateByUrl('agentaccount');
  }


}
