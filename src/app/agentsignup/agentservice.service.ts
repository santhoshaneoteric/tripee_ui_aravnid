import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Constants } from 'src/app/common/constants.enum';



@Injectable({
  providedIn: 'root'
})
export class AgentserviceService {

  constructor(private http: HttpClient) { }


  create(obj: object): Observable<any>  {
    return this.http.post(Constants.API_URL +'v1/auth/agent_sign_up', obj, {})
      .pipe(map((res: any) => res));
  }
  getSingleAgent(id:string): Observable<any>  {
    return this.http.get(Constants.API_URL +'v1/auth/agent/'+ id, {})
      .pipe(map((res: any) => res));
  }


}
