import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, EmailValidator } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import {ResetService} from 'src/app/resetpassword/reset.service'
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-resendmail',
  templateUrl: './resendmail.component.html',
  styleUrls: ['./resendmail.component.scss'],
  providers: [ ResetService]
})
export class ResendmailComponent implements OnInit {

  ResetForm:FormGroup;
  obj:any;

  constructor(private router: Router,private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,private activeRoute: ActivatedRoute,
    private resetservice:ResetService) { }


  ngOnInit() {
    
    this.ResetForm=this.formBuilder.group({
      
      email: ['',Validators.required]
     
    });
    
    
    
      }
      
     // console.log(otp_value)
      //let obj= {email:email,otp:otp_value}
      // onVerify() {
      //   console.log(this.OtpForm.value);
      //   this.verifyservice.verify(this.OtpForm.value).subscribe(data => {
          
         
      //   });
      // }
      onreset() {
        this.spinner.show();
        let email = localStorage.getItem("email");
        // console.log(email)
    
        //this.AgentForm.value.status = 'active';
        // let obj={"email":this.ResetForm.value.email}
        let obj={"email":email}
        console.log(obj)
        this.resetservice.reset(obj).subscribe(data => {
          this.spinner.hide();
          if (data) {
            console.log(data)
            //this.toastr.success(data.message, 'Trippe');
            this.router.navigateByUrl('verifyuseraccount');
          }
        });
      }

}
