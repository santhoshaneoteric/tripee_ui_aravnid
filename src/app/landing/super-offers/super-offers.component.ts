import { Component, OnInit, ViewChild } from '@angular/core';
import { HelperService } from '../../common/helper.service';

@Component({
  selector: 'app-super-offers',
  templateUrl: './super-offers.component.html',
  styleUrls: ['./super-offers.component.scss'],
  providers: [HelperService]
})
export class SuperOffersComponent implements OnInit {
  imageObject = [];
  constructor(private helperService: HelperService) { }

  ngOnInit() {
    this.imageObject = this.helperService.getImagesObject();
  }

}
