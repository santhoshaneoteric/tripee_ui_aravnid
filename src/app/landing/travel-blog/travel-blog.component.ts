import { Component, OnInit } from '@angular/core';
import { HelperService } from '../../common/helper.service';

@Component({
  selector: 'app-travel-blog',
  templateUrl: './travel-blog.component.html',
  styleUrls: ['./travel-blog.component.scss'],
  providers: [HelperService]
})
export class TravelBlogComponent implements OnInit {
  innoDemos = [];
  constructor(private helperService: HelperService) { }

  ngOnInit() {
    this.innoDemos = this.helperService.getInnoDemos();
  }

}
