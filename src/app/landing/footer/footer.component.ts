import { Component, OnInit } from '@angular/core';
import { HelperService } from '../../common/helper.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  providers: [HelperService]
})
export class FooterComponent implements OnInit {
  footerObj = [];
  constructor(private helperService: HelperService) { }

  ngOnInit() {
    this.footerObj = this.helperService.getFooterObj();
  }

}
