import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Constants } from '../common/constants.enum';

@Injectable({
  providedIn: 'root'
})
export class LandingService {

  constructor(private http: HttpClient) { }
  searchFlights(obj: object): Observable<any>  {
    return this.http.post(Constants.API_URL + 'flightsearch/domesticoneway/', obj, {})
      .pipe(map((res: any) => res));
  }
  FareRule(obj: object): Observable<any>  {
    return this.http.post(Constants.API_URL + 'flightsearch/farerule/', obj, {})
      .pipe(map((res: any) => res));
  }

}
