import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { SignupService } from '../signup/signup.service';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
  providers: [SignupService]
})
export class ChangePasswordComponent implements OnInit {
  changePassword: FormGroup;
  constructor(private fb: FormBuilder, private spinner: NgxSpinnerService,
              private toastr: ToastrService, private signupService: SignupService,
              private dialog: MatDialog) {
    this.changePassword = fb.group({
      oldPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  onChangePassword() {
    if (this.changePassword.value.newPassword !== this.changePassword.value.confirmPassword) {
      this.toastr.info('New Password and Confirm Password are not identical', 'Geturtrip');
      return;
    }
    this.signupService.changePassword(localStorage.getItem('userId'), this.changePassword.value).subscribe(data => {
      if (data) {
        this.toastr.success(data.message, 'Geturtrip');
        this.dialog.closeAll();
      }
    });
  }

  dialogClose() {
    this.dialog.closeAll();
  }

}
