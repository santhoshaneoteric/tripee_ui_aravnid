
import { Component, OnInit, Inject, HostListener } from '@angular/core';
import { LandingService } from 'src/app/landing/landing.service';
import { Router } from '@angular/router';
import { format } from 'date-fns';
import { NgxSpinnerService } from 'ngx-spinner';
import {HelperService} from 'src/app/common/helper.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';


@Component({
  selector: 'app-searchcomponent',
  templateUrl: './searchcomponent.component.html',
  styleUrls: ['./searchcomponent.component.scss'],
  providers:[LandingService]
})
export class SearchcomponentComponent implements OnInit {

  showReturnFlightDate;
  reqObjOneWay = {};
  originDestinations = [];
  Infants: any = '0';
  cabinClass;
  AirlineType;
  Adults: any = '1';
  //cabinClass;
  Childs: any = '0';
  //Adults = 0;
  //Childs = 0;
  //Infants = 0;
  origin = '';
  destination = '';
  departDate = '';
  todaysDate = new Date();
  returnDate;
  showMultiCity;
  tripJackMultiWay;
  selectedOption = 'oneWay';
  routeInfos;
  multiObject = [];
  oneWayActive = true;
  roundTripActive = false;
  multiCityActive = false;
  specialRoundActive = false;
  showMultiCityDiv;
  currentlyDisabled 
  
  cities = [{
    value: 'DEL',
    viewValue: 'Delhi'
  }, {
    value: 'BOM',
    viewValue: 'Mumbai'
  }, {
    value: 'BLR',
    viewValue: 'Bangalore'
  }];
 
  flightClass = [{
    value: 'ECONOMY',
    viewValue: 'Economy'
  }, {
    value: 'W',
    viewValue: 'Premium Economy'
  }, {
    value: 'C',
    viewValue: 'Business'
  }];
  airlineType = [{
    value: 'A',
    viewValue: 'All'
  }, {
    value: 'G',
    viewValue: 'Only GDS',
  }, {
    value: 'L',
    viewValue: 'Only LCC'
  }];
  stops: any = 0;
  constructor(private landingService: LandingService, private router: Router,
              private spinner: NgxSpinnerService,private helperService: HelperService,private dialog: MatDialog, private dialogRef: MatDialogRef<SearchcomponentComponent>,) { }

  ngOnInit() {
    
    this.originDestinations = this.helperService.originDestinations();
    this.cabinClass = this.flightClass[0].value;
    this.AirlineType = this.airlineType[0].value;
    
    this.multiObject.push(
      {
      fromCityOrAirport: '',
      toCityOrAirport: '',
      travelDate: '',
    },
    {
      fromCityOrAirport: '',
      toCityOrAirport: '',
      travelDate: '',
    }
    );
  }

  searchDialogClose() {
    this.dialogRef.close();
  }
  onTodaysDate() {
    if (this.departDate) {
      this.returnDate = new Date(this.departDate);
    }
  }

  onReturnDate() {
  

  }

  changeChipSelected(value) {
    this.selectedOption = value;
    switch (value) {
      case 'oneWay':
        // this.showReturnFlightDate = false;
        this.showMultiCityDiv = false;
        this.currentlyDisabled = true;
        this.oneWayActive = true;
        this.roundTripActive = false;
        this.multiCityActive = false;
        this.specialRoundActive = false;
        break;
      case 'multiCity':
        this.showMultiCityDiv = true;
        this.currentlyDisabled = false;
        this.oneWayActive = false;
        this.roundTripActive = false;
        this.multiCityActive = true;
        this.specialRoundActive = false;
        break;
      case 'roundTrip':
        // this.showReturnFlightDate = true;
        this.showMultiCityDiv = false;
        this.currentlyDisabled = false;
        this.oneWayActive = false;
        this.roundTripActive = true;
        this.multiCityActive = false;
        this.specialRoundActive = false;
        break;
      default:
        // this.showReturnFlightDate = true;
        this.showMultiCityDiv = false;
        this.currentlyDisabled = false;
        this.oneWayActive = false;
        this.roundTripActive = false;
        this.multiCityActive = false;
        this.specialRoundActive = true;
    }
  }

  onFlightSearch() {
    if (this.selectedOption === 'oneWay') {
      this.oneWayFlightSearch();
    } 
    else if (this.selectedOption === 'multiCity') {
    this.multiWaySearch();
    } 
    // else if (this.selectedOption === 'specialRoundTrip') {
    // this.specialRoundTrip();
    //  }
     else {
      this.roundTripSearch();
    }
  }

  oneWayFlightSearch() {
    localStorage.setItem('origin', this.origin);
    localStorage.setItem('destination', this.destination);
    localStorage.setItem('departDate', this.departDate);
    localStorage.setItem('Adults', this.Adults);
    localStorage.setItem('Childs', this.Childs);
    localStorage.setItem('Infants', this.Infants);
    localStorage.setItem('CabinClass', this.cabinClass);
    localStorage.setItem('AirlineType', this.AirlineType);
    if (this.departDate) {
      this.departDate = format(new Date(this.departDate), 'yyyy-MM-dd');
    }
    if (this.multiObject[0].travelDate) {
      this.multiObject.forEach(obj => {
        obj.fromCityOrAirport = {
          code : obj.fromCityOrAirport
        };
        obj.toCityOrAirport = {
          code : obj.toCityOrAirport
        };
        obj.travelDate = format(new Date(obj.travelDate), 'yyyy-MM-dd');
      });
    }
    this.routeInfos = [{
      fromCityOrAirport: {
        code: this.origin
      },
      toCityOrAirport: {
        code: this.destination
      },
      travelDate: this.departDate
    }];
    this.reqObjOneWay = {
      searchQuery: {
        cabinClass: this.cabinClass,
        paxInfo: {
          ADULT: this.Adults,
          CHILD: this.Childs,
          INFANT: this.Infants
        },
        routeInfos: this.routeInfos,
        searchModifiers: {
          isDirectFlight: true,
          isConnectingFlight: true
        }
      }
    };
   // localStorage.setItem(this.reqObjOneWay);
    this.spinner.show();
    this.landingService.searchFlights(this.reqObjOneWay).subscribe(data => {
 //     data.stops = this.stops;
      //console.log(data.content)
      if (data) {

        this.spinner.hide();

        
        localStorage.removeItem('resultData');
        localStorage.setItem('resultData', JSON.stringify(data));
        this.dialogRef.close();
        this.router.navigate(['search-results', this.selectedOption]);
      } else {
        this.spinner.hide();
      }
    });
  }
  
  roundTripSearch() {
    localStorage.setItem('origin', this.origin);
    localStorage.setItem('destination', this.destination);
    localStorage.setItem('departDate', this.departDate);
    localStorage.setItem('returnDate', this.returnDate);
    localStorage.setItem('Adults', this.Adults);
    localStorage.setItem('Childs', this.Childs);
    localStorage.setItem('Infants', this.Infants);
    localStorage.setItem('CabinClass', this.cabinClass);
    localStorage.setItem('AirlineType', this.AirlineType);
    if (this.departDate) {
      this.departDate = format(new Date(this.departDate), 'yyyy-MM-dd');
    }
    this.routeInfos = [{
      fromCityOrAirport: {
        code: this.origin
      },
      toCityOrAirport: {
        code: this.destination
      },
      travelDate: this.departDate
    }, {
      fromCityOrAirport: {
        code: this.destination
      },
      toCityOrAirport: {
        code: this.origin
      },
      travelDate: format(new Date(this.returnDate), 'yyyy-MM-dd')
    }];
    this.reqObjOneWay = {
      searchQuery: {
        cabinClass: this.cabinClass,
        paxInfo: {
          ADULT: this.Adults,
          CHILD: this.Childs || 0,
          INFANT: this.Infants || 0
        },
        routeInfos: this.routeInfos,
        searchModifiers: {
          isDirectFlight: true,
          isConnectingFlight: true
        },
        searchType: 'RETURN'
      }
    };
    localStorage.setItem('roundFromAirportCode', this.origin);
    localStorage.setItem('roundToAirportCode', this.destination);
    const finalObj = {
      tripJack: this.reqObjOneWay,
     
      type: 'ROUNDTRIP'
    };
    this.spinner.show();
    localStorage.removeItem('resultData');
    this.landingService.searchFlights(this.reqObjOneWay).subscribe(data => {
      //     data.stops = this.stops;
           //console.log(data.content)
           if (data) {
             this.spinner.hide();
             localStorage.setItem('resultData', JSON.stringify(data));
             
             this.router.navigate(['search-results', this.selectedOption]);
           } else {
             this.spinner.hide();
           }
         });

  }

  
  multiWaySearch() {
    const multiFromAirportArr = [];
    const multiToAirportArr = [];
    const mutliDepDateArr = [];
    this.multiObject.forEach(obj => {
      multiFromAirportArr.push(obj.fromCityOrAirport);
      multiToAirportArr.push(obj.toCityOrAirport);
      mutliDepDateArr.push(format(new Date(obj.travelDate), 'dd/MM/yyyy'));
    });
    localStorage.setItem('multiFromAirportCodes', JSON.stringify(multiFromAirportArr));
    localStorage.setItem('multiToAirportCodes', JSON.stringify(multiToAirportArr));
    localStorage.setItem('multiDateArr', JSON.stringify(mutliDepDateArr));
    if (this.multiObject[0].travelDate) {
      this.multiObject.forEach(obj => {
        obj.fromCityOrAirport = {
          code : obj.fromCityOrAirport
        };
        obj.toCityOrAirport = {
          code : obj.toCityOrAirport
        };
        obj.travelDate = format(new Date(obj.travelDate), 'yyyy-MM-dd');
      });
    }

    this.tripJackMultiWay = {
      searchQuery: {
        cabinClass: this.cabinClass,
        paxInfo: {
          ADULT: this.Adults,
          CHILD: this.Childs || 0,
          INFANT: 0
        },
        routeInfos: this.multiObject,
        searchModifiers: {
          isDirectFlight: true,
          isConnectingFlight: true
        },
        searchType: 'MULTICITY'
      }
    };
  
    const finalObj = {
      tripJack: this.tripJackMultiWay,
     
      type: 'MULTIWAY'
    };
    this.spinner.show();
    this.landingService.searchFlights(this.tripJackMultiWay).subscribe(data => {
      data.stops = this.stops;
      if (data) {
        this.spinner.hide();
        localStorage.setItem('resultData', JSON.stringify(data));
        this.router.navigate(['search-results', this.selectedOption]);
      } else {
        this.spinner.hide();
      }
    });
   
  }
  onOriginChange(value) {
    sessionStorage.setItem('prevOrigin', value);
  }

  onDestinationChange(value) {
    sessionStorage.setItem('prevDestination', value);
  }

  changeOriginDest() {
    this.origin = sessionStorage.getItem('prevDestination');
    this.destination = sessionStorage.getItem('prevOrigin');
    this.onOriginChange(this.origin);
    this.onDestinationChange(this.destination);
  }

  onAddMulti() {
    if (this.multiObject.length < 5) {
      this.multiObject.push({
        fromCityOrAirport: '',
        toCityOrAirport: '',
        travelDate: '',
      });
    }
  }

  onRemoveMulti(i: number) {
   if (this.multiObject.length > 1) {
    this.multiObject.splice(i, 1);
   }
  }



}
