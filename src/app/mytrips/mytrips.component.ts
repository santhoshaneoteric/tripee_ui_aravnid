import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';



export interface UserData {
  
BookingId:string;
Bookingdate:string;
name:string;
contact:string;
airline:string;
Triptype:string;
fare:string;
class:string;
Traveldate:string;
 
  
 
}
@Component({
  selector: 'app-mytrips',
  templateUrl: './mytrips.component.html',
  styleUrls: ['./mytrips.component.scss']
})
export class MytripsComponent implements OnInit {

  displayedColumns: string[] = ['BookingId','Bookingdate','name','contact','airline','Triptype','fare','class','Traveldate'];
  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private router: Router){ }
  ngOnInit() {
  }

}
