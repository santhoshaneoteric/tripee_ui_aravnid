
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as CryptoJS from 'crypto-js';
import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';


export interface PeriodicElement {
  name: string;
 
  
}

const ELEMENT_DATA: PeriodicElement[] = [
  {name: 'Santhosha'},
 

];




@Component({
  selector: 'app-mycoupons',
  templateUrl: './mycoupons.component.html',
  styleUrls: ['./mycoupons.component.scss']
})
export class MycouponsComponent implements OnInit {
 

  displayedColumns: string[] = [ 'name'];
  dataSource = ELEMENT_DATA;
  ngOnInit(){}
}