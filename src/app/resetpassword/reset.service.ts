import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Constants } from '../common/constants.enum';

@Injectable({
  providedIn: 'root'
})
export class ResetService {

  constructor(private http: HttpClient) { }
  reset(obj: object): Observable<any> {
    return this.http.post(Constants.API_URL + 'v1/auth/verify_email', obj, {})
      .pipe(map((res: any) => res));
  }
  resendactivation(obj: object): Observable<any> {
    return this.http.post(Constants.API_URL + 'v1/auth/resend_activation', obj, {})
      .pipe(map((res: any) => res));
  }

}
