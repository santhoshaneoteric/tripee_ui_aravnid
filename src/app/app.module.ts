import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from './shared/shared.module';
import { LandingComponent } from './landing/landing.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { SocialLoginModule, AuthServiceConfig } from 'angularx-social-login';
import { GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';
import { SuperOffersComponent } from './landing/super-offers/super-offers.component';
import { NgImageSliderModule } from 'ng-image-slider';
import { TravelBlogComponent } from './landing/travel-blog/travel-blog.component';
import { FooterComponent } from './landing/footer/footer.component';
import { FlightResultsComponent } from './+flight/flight-results/flight-results.component';
import { Ng5SliderModule } from 'ng5-slider';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FlightDetailsComponent } from './+flight/flight-details/flight-details.component';
import { RoundDialogComponent } from './+flight/round-dialog/round-dialog.component';
import { MytripsComponent } from 'src/app/mytrips/mytrips.component';
import { MytravellerComponent } from 'src/app/mytraveller/mytraveller.component';
import { MycouponsComponent } from 'src/app/mycoupons/mycoupons.component';
import { MyprofileComponent } from 'src/app/myprofile/myprofile.component';
import {MatPaginatorModule} from '@angular/material';
import { AgentaccountComponent } from 'src/app/agentaccount/agentaccount.component';
import { AgentsignupComponent } from './agentsignup/agentsignup.component';
//import { AgentcreditComponent } from './agent/agentcredit/agentcredit.component';
import { ToastrService } from 'ngx-toastr';
import { VerifyuseraccountComponent } from './verifyuseraccount/verifyuseraccount.component';
import { ChangePasswordComponent } from 'src/app/change-password/change-password.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component'

import { AgentDashboardComponent } from './agent/agent-dashboard/agent-dashboard.component';
import { AgentFlightsComponent } from 'src/app/agent/agent-dashboard/agent-flights/agent-flights.component';
import { AgentHotelsComponent } from 'src/app/agent/agent-dashboard/agent-hotels/agent-hotels.component';
//import { AgentMarkupManagementComponent } from './agent/agent-markup-management/agent-markup-management.component';
import { AgentprofileComponent } from 'src/app/agent/agent-dashboard/agentprofile/agentprofile.component';
import { FlightmarkupComponent } from 'src/app/agent/agent-dashboard/flightmarkup/flightmarkup.component';
import { HotelmarkupComponent } from 'src/app/agent/agent-dashboard/hotelmarkup/hotelmarkup.component';
import { AdmindashboardComponent } from './admin/admin-dashboard/admindashboard/admindashboard.component';
import { CreateAgentComponent } from 'src/app/admin/admin-dashboard/admindashboard/b2b-user-management/create-agent/create-agent.component';
import { ManageAgentComponent } from 'src/app/admin/admin-dashboard/admindashboard/b2b-user-management/manage-agent/manage-agent.component';
import { AdminLoginComponent } from './admin/admin-login/admin-login.component';
import { ToastrModule } from 'ngx-toastr';
import { HotelMarkupManagerComponent } from 'src/app/admin/admin-dashboard/admindashboard/b2b-markup-manager/hotel-markup-manager/hotel-markup-manager.component';
// tslint:disable-next-line: max-line-length
import { FlightMarkupManagerComponent } from 'src/app/admin/admin-dashboard/admindashboard/b2b-markup-manager/flight-markup-manager/flight-markup-manager.component';


import { HotelReportComponent } from 'src/app/admin/admin-dashboard/admindashboard/b2b-report-manager/hotel-report/hotel-report.component';
import { FlightReportComponent } from 'src/app/admin/admin-dashboard/admindashboard/b2b-report-manager/flight-report/flight-report.component';
import { HotelUserMarkupManagerComponent } from 'src/app/admin/admin-dashboard/admindashboard/b2c-markup-manager/hotel-user-markup-manager/hotel-user-markup-manager.component';

import { FlightUserMarkupManagerComponent } from 'src/app/admin/admin-dashboard/admindashboard/b2c-markup-manager/flight-user-markup-manager/flight-user-markup-manager.component';
import { HotelUserReportComponent } from 'src/app/admin/admin-dashboard/admindashboard/b2c-report-manager/hotel-user-report/hotel-user-report.component';
import { FlightUserReportComponent } from 'src/app/admin/admin-dashboard/admindashboard/b2c-report-manager/flight-user-report/flight-user-report.component';
import { ManageUserComponent } from 'src/app/admin/admin-dashboard/admindashboard/b2c-user-management/manage-user/manage-user.component';
import { VoucherComponent } from 'src/app/admin/admin-dashboard/admindashboard/voucher/voucher.component';



import { SalesReportComponent } from 'src/app/admin/admin-dashboard/admindashboard/reports/sales-report/sales-report.component';


import { PaymentCancellationComponent } from 'src/app/admin/admin-dashboard/admindashboard/reports/payment-cancellation/payment-cancellation.component';
import { BookingReportComponent } from 'src/app/admin/admin-dashboard/admindashboard/reports/booking-report/booking-report.component';
import { PendingBookingComponent } from 'src/app/admin/admin-dashboard/admindashboard/reports/pending-booking/pending-booking.component';
import { RefundReportComponent } from 'src/app/admin/admin-dashboard/admindashboard/reports/refund-report/refund-report.component';
import { CancellationReportComponent } from 'src/app/admin/admin-dashboard/admindashboard/reports/cancellation-report/cancellation-report.component';
import { OrderModule } from 'ngx-order-pipe';
import { ResendmailComponent } from './resendmail/resendmail.component';
import { VerifyComponent } from './verify/verify.component';
import { SearchcomponentComponent } from './searchcomponent/searchcomponent.component';
import { PhoneotpComponent } from './phoneotp/phoneotp.component';
import { CarresultsComponent } from './Cars/carresults/carresults.component';
import { FlightReviewComponent } from './+flight/flight-review/flight-review.component';
import{BottonSheetComponent} from 'src/app/+flight/flight-results/botton-sheet/botton-sheet.component';
import { RoundflightreviewComponent } from './+flight/roundflightreview/roundflightreview.component';
import { RoundtripFlightdetailsComponent } from './+flight/roundtrip-flightdetails/roundtrip-flightdetails.component'
import {MatExpansionModule} from '@angular/material/expansion';
const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('252676682909-b2luuin9c2defrblhbfiv0hv58255uaq.apps.googleusercontent.com')
  }
  // {
  //   id: FacebookLoginProvider.PROVIDER_ID,
  //   provider: new FacebookLoginProvider('Facebook-App-Id')
  // }
]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    LoginComponent,
    SignupComponent,
    SuperOffersComponent,
    TravelBlogComponent,
    FooterComponent,
    FlightResultsComponent,
    FlightDetailsComponent,
    RoundDialogComponent,
    MytripsComponent,
    MytravellerComponent,
    MycouponsComponent,
    MyprofileComponent,
    AgentaccountComponent,
    AgentsignupComponent,
    //AgentcreditComponent,
    //AgentMarkupManagementComponent ,
    AgentFlightsComponent ,
    AgentHotelsComponent ,
    AgentDashboardComponent,
    VerifyuseraccountComponent ,
    ChangePasswordComponent,
    ResetpasswordComponent,
    AdmindashboardComponent,
    CreateAgentComponent,
    ManageAgentComponent ,
    AdminLoginComponent,
    HotelMarkupManagerComponent,
    FlightMarkupManagerComponent,
    SalesReportComponent,
    BookingReportComponent ,
    PendingBookingComponent ,
    RefundReportComponent,
    CancellationReportComponent,
    HotelUserReportComponent,
    FlightUserReportComponent,


    BottonSheetComponent,
    HotelReportComponent,
    FlightReportComponent,
    HotelUserMarkupManagerComponent,
    FlightUserMarkupManagerComponent,
    ManageUserComponent,
    VoucherComponent,
    ResendmailComponent,
    VerifyComponent,
    SearchcomponentComponent,
    PhoneotpComponent,
    CarresultsComponent,
    FlightReviewComponent,
    RoundflightreviewComponent,
    RoundtripFlightdetailsComponent,











  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    HttpClientModule,
    NgbModule,
    SocialLoginModule,
    NgImageSliderModule,
    Ng5SliderModule,
    NgxSpinnerModule,
    MatPaginatorModule,
    OrderModule,
    MatExpansionModule,
    ToastrModule.forRoot()

  ],
  providers: [{
    provide: AuthServiceConfig,
    useFactory: provideConfig,
    // provide:ToastrService,
    // useClass: ToastrService
  }],
  entryComponents: [LoginComponent, SignupComponent,VerifyComponent, VerifyuseraccountComponent ,SearchcomponentComponent,PhoneotpComponent,BottonSheetComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
