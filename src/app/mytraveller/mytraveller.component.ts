import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as CryptoJS from 'crypto-js';
import { ViewChild } from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';



export interface UserData {
 
  sno:string;
  name:string;
  dob:string;

  Passport:string;
  doe:string;
  Status:string;
 
}
@Component({
  selector: 'app-mytraveller',
  templateUrl: './mytraveller.component.html',
  styleUrls: ['./mytraveller.component.scss']
})
export class MytravellerComponent implements OnInit {

  displayedColumns: string[] = ['sno','name','dob','Passport','doe','Status'];
  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  
  
 myTravellerform: FormGroup;
  adminStatus;
  
  
  constructor(private router: Router, 
              private formBuilder: FormBuilder, 
              private activeRoute: ActivatedRoute) { }

  
      ngOnInit() {
        this.myTravellerform = this.formBuilder.group({
          firstName:['',Validators.required],
          lastName:['',Validators.required],
          email:['',Validators.required],
          phone:['',Validators.required],
          gender:['',Validators.required],
          
          passportnumber:['',Validators.required],
          issuingcountry:['',Validators.required],
          expirydate:['',Validators.required],
          birthday:['',Validators.required],
          name:['',Validators]

         
         
        });
        
        
    
    
          
         

    
    }
}