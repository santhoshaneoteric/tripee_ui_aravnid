import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MytravellerComponent } from './mytraveller.component';

describe('MytravellerComponent', () => {
  let component: MytravellerComponent;
  let fixture: ComponentFixture<MytravellerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MytravellerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MytravellerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
