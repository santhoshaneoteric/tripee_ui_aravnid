import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LoginComponent } from './login/login.component';
import { JwtHelperService } from '@auth0/angular-jwt';
import {  OnInit, HostListener, OnDestroy } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { CommonService } from './common/common.service';
import { Subscription } from 'rxjs';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('fade',
    [
      state('void', style({ opacity : 0})),
      transition(':enter', [ animate(300)]),
      transition(':leave', [ animate(500)]),
    ]
)]
})
export class AppComponent {
  userLoggedIn;
  subscription: Subscription;
  username;
  showHideHeaderFooter;

  // title = 'tripee-ui';
  constructor(private dialog: MatDialog,private router: Router, private commonService: CommonService) {}
  navItems = [{
    navIcon: 'flight_takeoff',
    navLabel: 'Flights'
  }, {
    navIcon: 'hotels',
    navLabel: 'Hotels'
  }, {
    navIcon: 'directions_bus',
    navLabel: 'Buses'
  }, {
    navIcon: 'directions_car',
    navLabel: 'Cabs'
  }, {
    navIcon: 'person',
    navLabel: 'Visas'
  }, {
    navIcon: 'home',
    navLabel: 'Holidays'
  }, {
    navIcon: 'category',
    navLabel: 'Group'
  }];
  ngOnInit(){

    this.router.events.pipe(filter((event: any) => event instanceof NavigationEnd)).subscribe(event => {
      if (event.url.indexOf('admin') > -1) {
        this.showHideHeaderFooter = true;
      } else if (event.url.indexOf('admindashboard') > -1) {
        this.showHideHeaderFooter = true;
      } else if (event.url.indexOf('agentdashboard') > -1) {
        this.showHideHeaderFooter = true;
      } else if (event.url.indexOf('agentaccount') > -1) {
        this.showHideHeaderFooter = true;
      } 
      else if (event.url.indexOf('agentsignup') > -1) {
        this.showHideHeaderFooter = true;
      } 
      else {
        this.showHideHeaderFooter = false;
      }
    });
    const tokenhelper = new JwtHelperService();
    console.log(tokenhelper)
    let tok=localStorage.getItem('token')
    console.log(tok)
    const isExpired = tokenhelper.isTokenExpired(localStorage.getItem('token'));
    console.log(isExpired)
    const isDecode=tokenhelper.decodeToken(localStorage.getItem('token'));
    // console.log(isDecode)
    // console.log(isDecode.username)
    
    let nami=localStorage.getItem('username')
    console.log(nami)
    let idbase=localStorage.setItem("userId",isDecode.user_id)
    console.log(idbase)
    
    // this.subscription = this.commonService.getUserInfo().subscribe(obj => {
    //   console.log(obj);
    //   if (obj.username) {
    //     console.log(obj);
    //     this.username = obj.username;
    //     this.userLoggedIn = true;
    //   } else {
    //     // clear messages when empty message received
    //     // this.messages = [];
    //   }
    // // });
    // if(localStorage.getItem('token') && isExpired){
    //   onSignout();
    // }
    
    if (localStorage.getItem('token') && !isExpired) {
      this.username = localStorage.getItem('username');
      console.log(this.username)
    
      this.userLoggedIn = true;
    } else {
      localStorage.removeItem('token');
      localStorage.removeItem('username');

      this.userLoggedIn = false;
    }

  }
  openLoginDialog() {
    const dialogRef = this.dialog.open(LoginComponent, {
      disableClose: true,
      panelClass: 'login-container'
    });
    dialogRef.afterClosed().subscribe(data => {
      console.log(data);
      if (data) {
        // this.userLoggedIn = true;
        // this.userDetails = data;
        // localStorage.setItem('userExists', JSON.stringify(data));
      }
    });
  }
  onSignout() {
    localStorage.removeItem('token');
   // localStorage.removeItem('name');
    localStorage.removeItem('userId');
    this.userLoggedIn = false;
    this.router.navigateByUrl('home');

  }
  onMyProfile() {
    this.router.navigateByUrl('myprofile');
  }
  onMyTrips() {
    this.router.navigateByUrl('mytrips');
  }
  // onMyTraveller() {
  //   this.router.navigateByUrl('mytraveller');
  // }
  onMyCoupons() {
    this.router.navigateByUrl('mycoupons');
  }
  
}
