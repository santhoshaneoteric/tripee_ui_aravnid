import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyuseraccountComponent } from './verifyuseraccount.component';

describe('VerifyuseraccountComponent', () => {
  let component: VerifyuseraccountComponent;
  let fixture: ComponentFixture<VerifyuseraccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyuseraccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyuseraccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
