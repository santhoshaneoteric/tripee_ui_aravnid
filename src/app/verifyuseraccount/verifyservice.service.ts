import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Constants } from '../common/constants.enum';

@Injectable({
  providedIn: 'root'
})
export class VerifyserviceService {
  constructor(private http: HttpClient) { }
  verify(obj: object): Observable<any> {
    return this.http.post(Constants.API_URL + 'v1/auth/verify_otp_reset', obj, {})
      .pipe(map((res: any) => res));
  }
  verify_aftrelogin(obj: object): Observable<any> {
    return this.http.post(Constants.API_URL + 'v1/auth/verify_otp', obj, {})
      .pipe(map((res: any) => res));
  }
  email_for_activation(obj: object): Observable<any> {
    return this.http.post(Constants.API_URL + 'v1/auth/emailtoverify', obj, {})
      .pipe(map((res: any) => res));
  }

 
 
}
