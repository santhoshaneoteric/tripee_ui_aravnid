import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, EmailValidator } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import {VerifyserviceService} from 'src/app/verifyuseraccount/verifyservice.service'
//import { ToastrService } from 'ngx-toastr';
//import { ToastrService } from 'ngx-toastr';
import {ResetService} from 'src/app/resetpassword/reset.service'
import { MatDialog, MatDialogRef  } from '@angular/material/dialog';
import { LoginComponent } from 'src/app/login/login.component';

@Component({
  selector: 'app-verifyuseraccount',
  templateUrl: './verifyuseraccount.component.html',
  styleUrls: ['./verifyuseraccount.component.scss'],
  providers: [ VerifyserviceService,ResetService]
})
export class VerifyuseraccountComponent implements OnInit {
  OtpForm:FormGroup;
  obj:any;

  constructor(private router: Router,private formBuilder: FormBuilder,private dialogRef: MatDialogRef<VerifyuseraccountComponent>,
    private spinner: NgxSpinnerService,private activeRoute: ActivatedRoute,
    private verifyservice:VerifyserviceService,private resetservice:ResetService) { }


  ngOnInit() {
    
    this.OtpForm=this.formBuilder.group({
      //email:['',Validators.required],
      
      otp: ['',Validators.required]
     
    });
    
    
    
      }
      
     // console.log(otp_value)
      //let obj= {email:email,otp:otp_value}
      // onVerify() {
      //   console.log(this.OtpForm.value);
      //   this.verifyservice.verify(this.OtpForm.value).subscribe(data => {
          
         
      //   });
      // }
      emailDialogClose() {
        this.dialogRef.close();
      }
    
      onVerify() {
        this.spinner.show();
        let email = localStorage.getItem('email');
         console.log(email)
    
        // //this.AgentForm.value.status = 'active';
        let obj={"email":email,"otp":this.OtpForm.value.otp}
         console.log(obj)
        console.log(this.OtpForm.value)
        this.verifyservice.verify_aftrelogin(obj).subscribe(data => {
          this.spinner.hide();
          if (data) {
            console.log(data)
            this.dialogRef.close();
            //this.toastr.success(data.message, 'Trippe');
            //this.router.navigateByUrl('myprofile');
          }
        });
      }
      Resendmail(){
       
          this.spinner.show();
          let email = localStorage.getItem("email");
          // console.log(email)
      
          //this.AgentForm.value.status = 'active';
          // let obj={"email":this.ResetForm.value.email}
          let obj={"email":email}
          console.log(obj)
          this.resetservice.resendactivation(obj).subscribe(data => {
            this.spinner.hide();
            if (data) {
              console.log(data)
              //this.toastr.success(data.message, 'Trippe');
              this.router.navigateByUrl('verifyuseraccount');
 

            }
          });
        
  
      }

}
