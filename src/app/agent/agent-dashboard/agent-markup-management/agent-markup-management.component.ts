import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import { Router, ActivatedRoute } from '@angular/router';

import { FormGroup, FormControl,FormBuilder, Validators } from '@angular/forms';

import * as CryptoJS from 'crypto-js';




export interface UserData {

  APIName:string;

  Markup :string;
  MarkupProcess:string;
  UpdatedDate:string;
  Status:string;
  Actions:string;
}

@Component({
  selector: 'app-agent-markup-management',
  templateUrl: './agent-markup-management.component.html',
  styleUrls: ['./agent-markup-management.component.scss']
})
export class AgentMarkupManagementComponent implements OnInit {

  displayedColumns: string[] = [
    'RefNo', 'PNR','Domestic','Destination','Departure','Action'
  ];
  form = new FormGroup({
    pnrstatus: new FormControl('', Validators.required),
    referencenumber: new FormControl('', Validators.required),
    email: new FormControl('', [
      Validators.required,
      Validators.email
    ]),

    phone: new FormControl('', [
      Validators.required,
      Validators.maxLength(10)
    ]),



   });


  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private router: Router) { }


  ngOnInit() {
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  onSubmit(){
    // alert(JSON.stringify(this.form.value));
  }



}
