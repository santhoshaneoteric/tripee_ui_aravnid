import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentMarkupManagementComponent } from './agent-markup-management.component';

describe('AgentMarkupManagementComponent', () => {
  let component: AgentMarkupManagementComponent;
  let fixture: ComponentFixture<AgentMarkupManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentMarkupManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentMarkupManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
