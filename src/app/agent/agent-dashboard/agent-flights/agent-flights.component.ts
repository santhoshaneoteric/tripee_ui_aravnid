import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import { Router, ActivatedRoute } from '@angular/router';

import { FormGroup, FormControl,FormBuilder, Validators } from '@angular/forms';

import * as CryptoJS from 'crypto-js';




export interface UserData {

  APIName:string;
  
  Markup :string;
  MarkupProcess:string;
  UpdatedDate:string;
  Status:string;
  Actions:string;
}

@Component({
  selector: 'app-agent-flights',
  templateUrl: './agent-flights.component.html',
  styleUrls: ['./agent-flights.component.scss']
})
export class AgentFlightsComponent implements OnInit {

countries=['India','UK','USA'];
 API=['Air India','Indigo','Spicejet']
 Markup=['Fixed','Percent']
 form = new FormGroup({
  Markup: new FormControl('', Validators.required),

  
 });
  displayedColumns: string[] = ['APIName','Markup','MarkupProcess','UpdatedDate','Status','Actions']
  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private router: Router) {}


  ngOnInit() {
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  
}
