import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentFlightsComponent } from './agent-flights.component';

describe('AgentFlightsComponent', () => {
  let component: AgentFlightsComponent;
  let fixture: ComponentFixture<AgentFlightsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentFlightsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentFlightsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
