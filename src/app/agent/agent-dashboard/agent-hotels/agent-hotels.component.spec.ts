import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentHotelsComponent } from './agent-hotels.component';

describe('AgentHotelsComponent', () => {
  let component: AgentHotelsComponent;
  let fixture: ComponentFixture<AgentHotelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentHotelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentHotelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
