import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import { stringify } from '@angular/compiler/src/util';
import { FormGroup, FormControl,FormBuilder, Validators } from '@angular/forms';



export interface UserData {

API:string;
HotelPNR:string;
Noofrooms:number;
Status:string;
AdminMarkup:string;
TotalPrice:number;
Action:string;

}

@Component({
  selector: 'app-agent-hotels',
  templateUrl: './agent-hotels.component.html',
  styleUrls: ['./agent-hotels.component.scss']
})
export class AgentHotelsComponent implements OnInit {

  displayedColumns: string[] = ['API','HotelPNR',
   'Noofrooms','Status','AdminMarkup','TotalPrice','Action'];

   form = new FormGroup({
    pnrstatus: new FormControl('', Validators.required),
    referencenumber: new FormControl('', Validators.required),
    email: new FormControl('', [
      Validators.required,
      Validators.email
    ]),

    phone: new FormControl('', [
      Validators.required,
      Validators.maxLength(10)
    ]),

   });

  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private router: Router,private formBuilder: FormBuilder) { }


  ngOnInit() {


  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

onSubmit(){
  // alert(JSON.stringify(this.form.value));
}
}
