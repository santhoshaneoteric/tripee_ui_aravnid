import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SignupService } from 'src/app/signup/signup.service';
import * as CryptoJS from 'crypto-js';
import { Constants } from 'src/app/common/constants.enum';
import { NgxSpinnerService } from 'ngx-spinner';
//import { ToastrService } from 'ngx-toastr';
import { AgentserviceService} from 'src/app/agentsignup/agentservice.service';


@Component({
  selector: 'app-agentprofile',
  templateUrl: './agentprofile.component.html',
  styleUrls: ['./agentprofile.component.scss']
})
export class AgentprofileComponent implements OnInit {
  AgentForm:FormGroup;
  submitBtnFlag;
  policyFile: any;
  policyFileName: string;
  formData;
  url;
  certificate;
  selectedCountry: String = "--Choose Country--";
  
	Countries: Array<any> = [
		{ name: 'Germany', states: [ {name: 'Baveria', cities: ['Duesseldorf', 'Leinfelden-Echterdingen', 'Eschborn']} ] },
		{ name: 'Spain', states: [ {name: 'Barcelona', cities: ['Barcelona']} ] },
		{ name: 'USA', states: [ {name: 'California', cities: ['Downers Grove']} ] },
		{ name: 'Mexico', states: [ {name: 'mexicostate', cities: ['Puebla']} ] },
		{ name: 'India', states: [ {name: ['AndhraPradesh'], cities: ['Vijaywada', 'Vizag', 'Kurnool', 'Chittoor','Tirupathy']},{name:['Telangana'],cities:['Hyderabad','Nalgonda','Karimnagar']} ] },
	];
  
	states: Array<any>;

	cities: Array<any>;

  constructor(private router: Router,private formBuilder: FormBuilder,private agentserviceService: AgentserviceService,
    private spinner: NgxSpinnerService,private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.AgentForm=this.formBuilder.group({
      
        name: ['', ],
        email: [''],
        password: ['', ],
        phone: ['', ],
  
        companyLocation: ['', ],
        officeNo: ['', ],
        companyWebsite: [''],
        companypancard: ['', ],
        gstno: ['', ],
       // uploadcertificate:['',Validators.required],
       // ownername: [''],
       // ownerAadhar: [''],
       // ownerpancard: [''],
        //ownerphone: [''],
        //owneremail: [''],
      
  
        country: ['', ],
        state: ['', ],
  
        city: ['', ],
        zipCode: ['',],
        //agentNo: ['',Validators.required],
        agentOrCompanyName: ['', ],
        //role:['Agent']

    });
    
    // this.createAgentForm.patchValue({
    //   currency: this.countries[0]
    // });
    // this.createAgentForm.patchValue({
    //   currency: this.states[0]
    // });
    // this.createAgentForm.patchValue({
    //   currency: this.cities[0]
    // });
    let userId = localStorage.getItem("userId");
    let obj = {id : userId}
    console.log(obj.id)
    this.agentserviceService.getSingleAgent(userId).subscribe(data => {
      if (data) {
        console.log(data)
        this.AgentForm.patchValue({
          email: data.email,
          //agentNo: data.id,
          name:data.name,
         // zipCode:data.agent[0].zipCode,

          // agentOrCompanyName: data.data.agentOrCompanyName,
          // companyLogo: data.data.companyLogo,
          // companyWebsite: data.data.companyWebsite,
          // firstName: data.data.firstName,
          // lastName: data.data.lastName,
          // companyLocation: data.data.companyLocation,
          // contactNo: data.data.contactNo,
          // officeNo: data.data.officeNo,
          // faxNo: data.data.faxNo,
          // zipCode: data.data.zipCode,
          // city: data.data.city,
          // state: data.data.state,
          // country: data.data.country,
          // taxNo: data.data.taxNo,
          // panNo: data.data.panNo
        });
       // this.logo = data.data.companyLogo;
      }
    });
  }

	
	changeCountry(country) {
    this.states = this.Countries.find(cntry => cntry.name == country).states;
    this.cities=this.Countries.find(cntry=> cntry.name == country).cities;
	}

	changeState(state) {
		this.cities = this.Countries.find(cntry => cntry.name == this.selectedCountry).states.find(stat => stat.name == state).cities;
	}
 





}
