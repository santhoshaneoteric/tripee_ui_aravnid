import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightmarkupComponent } from './flightmarkup.component';

describe('FlightmarkupComponent', () => {
  let component: FlightmarkupComponent;
  let fixture: ComponentFixture<FlightmarkupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightmarkupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightmarkupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
