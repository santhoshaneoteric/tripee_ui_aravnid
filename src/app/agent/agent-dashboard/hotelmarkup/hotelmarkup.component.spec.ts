import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotelmarkupComponent } from './hotelmarkup.component';

describe('HotelmarkupComponent', () => {
  let component: HotelmarkupComponent;
  let fixture: ComponentFixture<HotelmarkupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotelmarkupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelmarkupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
