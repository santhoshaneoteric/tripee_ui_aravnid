import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { HelperService } from '../../common/helper.service';

@Component({
  selector: 'app-agent-dashboard',
  templateUrl: './agent-dashboard.component.html',
  styleUrls: ['./agent-dashboard.component.scss']
})
export class AgentDashboardComponent implements OnInit {
  openedNav;
  showNavLabels = false;
  adminNavItems = [];
  adminTitle = 'Dashboard';
  username;
  @ViewChild('mySidenav', {static: true}) mySidenav: ElementRef;
  @ViewChild('main', {static: true}) main: ElementRef;
  constructor(private router: Router, private helperService: HelperService) { }

  ngOnInit() {
    this.adminNavItems = this.helperService.getAgentNavItems();
   this.username=localStorage.getItem('username')

  }

  openNav() {
    this.openedNav = false;
    this.showNavLabels = false;
    this.mySidenav.nativeElement.style.width = 'calc(100% - 75%)';
    this.main.nativeElement.style.width = 'calc(100% - 25%)';
  }

  closeNav() {
    this.openedNav = true;
    this.showNavLabels = true;
    this.mySidenav.nativeElement.style.width = 'calc(100% - 95%)';
    this.main.nativeElement.style.width = 'calc(100% - 5%)';
  }

  onRoles(obj) {
    if (obj.navUrl) {
      this.adminTitle = obj.navLabel;
      this.router.navigate(['agentdashboard/' + obj.navUrl]);
    }
  }
  onMyProfile(){
    this.router.navigateByUrl('agentprofile');


  }
  onSubRoles(obj) {
    this.adminTitle = obj.subNavLabel;
    this.router.navigate(['agentdashboard/' + obj.subNavUrl]);
  }

}
