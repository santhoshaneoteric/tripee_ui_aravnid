import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { SignupComponent } from '../signup/signup.component';
import { LoginService } from './login.service';
import { AuthService } from 'angularx-social-login';
import { FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';
import { HelperService } from '@app/common/helper.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import * as CryptoJS from 'crypto-js';
import { Constants } from '../common/constants.enum';
import { CommonService } from 'src/app/common/common.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {
  showFormFields = false;
  loginForm: FormGroup;
  constructor(private dialog: MatDialog, private dialogRef: MatDialogRef<LoginComponent>,
              private loginService: LoginService, private authService: AuthService, private fb: FormBuilder,
              private router: Router,    private helperService: HelperService,private commonService:CommonService) { }

  ngOnInit() {
    this.loginForm = this.fb.group({

      email: ['', [
        Validators.required,
        Validators.pattern('^[A-Za-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
      ]],
      password:['', [Validators.required, Validators.minLength(6)]],
      role:['user']
    });
  }

  openSignupDialog() {
    this.dialog.open(SignupComponent, {
      disableClose: true,
      panelClass: 'login-container'
    });
  }

  loginDialogClose() {
    this.dialogRef.close();
  }

  onTripeeAccount() {
   // this.showFormFields = true;
   
  
}

  onPersonalAccount() {
    this.showFormFields = false;
  }

  onlogin() {
    // tslint:disable-next-line: no-string-literal
   // const encryptedPswd = this.encrypt(this.loginForm.value['password']);
    this.loginService.login({email: this.loginForm.value.email, password: this.loginForm.value.password,role:this.loginForm.value.role}).subscribe((data) => {
      if (data) {
        console.log(data.token);
        console.log(data.name);
         localStorage.setItem("token", data.token);
         localStorage.setItem("username", data.name);
         this.commonService.saveUserInfo(data);
        // const tokenhelper = new JwtHelperService();
        // console.log(tokenhelper)
        // let tik=localStorage.setItem("token",data.token)
        // console.log(tik)
        // const isExpired = tokenhelper.isTokenExpired(localStorage.getItem('token'));
        //   console.log(isExpired)
        //   const isDecode=tokenhelper.decodeToken(localStorage.getItem('token'));
        //   console.log(isDecode)
        //   console.log(isDecode.username)
          
        //   let nami=localStorage.setItem("username",isDecode.username)
        //   console.log(nami)
        //   let idbase=localStorage.setItem("userId",isDecode.user_id)
        //   console.log(idbase)
          

       // let nam=localStorage.setItem("username",data.username)

        // console.log(nam)
        // this.commonService.saveUserInfo(data);
        
        this.dialogRef.close();
        //this.router.navigateByUrl('home');

      }
    });
  }
 
  onGoogleLogin() {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
    this.authService.authState.subscribe((user) => {
      if (user) {
        this.dialogRef.close(user);
      }
    });
  }

}
