import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentaccountComponent } from './agentaccount.component';

describe('AgentaccountComponent', () => {
  let component: AgentaccountComponent;
  let fixture: ComponentFixture<AgentaccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentaccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentaccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
