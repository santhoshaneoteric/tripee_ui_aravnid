import { TestBed } from '@angular/core/testing';

import { AgentloginService } from './agentlogin.service';

describe('AgentloginService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AgentloginService = TestBed.get(AgentloginService);
    expect(service).toBeTruthy();
  });
});
