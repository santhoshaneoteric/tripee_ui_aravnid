import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { HelperService } from 'src/app/common/helper.service';
import * as CryptoJS from 'crypto-js';
import { Constants } from 'src/app/common/constants.enum';
import { AgentloginService } from 'src/app/agentaccount/agentlogin.service';
import { JwtHelperService } from '@auth0/angular-jwt';
@Component({
  selector: 'app-agentaccount',
  templateUrl: './agentaccount.component.html',
  styleUrls: ['./agentaccount.component.scss'],
  providers: [AgentloginService]

})
export class AgentaccountComponent implements OnInit {

  hide = true;
  loginForm: FormGroup;
  constructor(private router: Router, private fb: FormBuilder,private agentService: AgentloginService,
              private spinner: NgxSpinnerService,private toastr:ToastrService,
              private helper: HelperService) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['',
      [
        Validators.required,
        Validators.pattern('^[A-Za-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
      ]],
      password:['', [Validators.required, Validators.minLength(6)]],
      role:['Agent']
    });
  }

  onSignin() {
    this.spinner.show();
    console.log(this.loginForm.value)
    //const encryptedPswd = CryptoJS.AES.encrypt(this.loginForm.value.password, Constants.CRYPTO_KEY);
    //this.loginForm.value.password = encryptedPswd.toString();
    this.agentService.login(this.loginForm.value).subscribe((data) => {
      console.log(data);
      this.spinner.hide();
      //this.loginForm.reset();
      if (data) {
        console.log(data)
        if (data) {
         // this.toastr.success(data.message, 'Geturtrip');
          localStorage.setItem("token", data.token);
          const tokenhelper = new JwtHelperService();
          console.log(tokenhelper)
          let tok=localStorage.getItem('token')
          console.log(tok)
          const isExpired = tokenhelper.isTokenExpired(localStorage.getItem('token'));
          console.log(isExpired)
          const isDecode=tokenhelper.decodeToken(localStorage.getItem('token'));
         // console.log(isDecode)
         // console.log(isDecode.username)
          
          let nami=localStorage.setItem("username", data.name);
          console.log(nami)
          let idbase=localStorage.setItem("userId",isDecode.user_id)
          console.log(idbase)
          //console.log(localStorage.setItem('loginId', data.user_id));
          //localStorage.setItem('role', );
         // let tik=localStorage.setItem("token",data.token)
        //  console.log(tik)
          this.router.navigateByUrl('agentdashboard');
        } else {
          this.toastr.info('Please contact admin','trippe');
        }
      } else {
       this.toastr.info(data.message, 'Geturtrip');
      }
    });
  }
}
