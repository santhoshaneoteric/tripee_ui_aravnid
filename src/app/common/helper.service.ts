import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HelperService {
  constructor() { }
  getNavItems() {
    return [
      {
        navIcon: 'home',
        navLabel: 'Home'
      },
      {
        navIcon: 'flight',
        navLabel: 'Flights'
      },
      {
        navIcon: 'hotel',
        navLabel: 'Hotels'
      },
      {
        navIcon: 'local_mall',
        navLabel: 'Holidays'
      },
      {
        navIcon: 'local_offer',
        navLabel: 'Offers'
      },
      {
        navIcon: 'contacts',
        navLabel: 'Contact Us'
      }
    ];
  }
  getMyAccount(){
    return [
      {
        navIcon: 'fa fa-user-circle',
        navLabel: 'My Profile',
        navUrl: 'myprofile',
        isDisabled: true,
        toggle: true
      } ,
      {
        navIcon: 'fa fa-suitcase',
        navLabel: 'My Trips',
        navUrl: 'mytrips',
        isDisabled: true,
        toggle: true
      } ,
      {
        navIcon: 'fa fa-user',
        navLabel: 'My Travellers',
        navUrl: 'mytraveller',
        isDisabled: true,
        toggle: true
      },
      {
        navIcon: 'fa fa-cc',
        navLabel: 'My Coupons',
        navUrl: 'mycoupons',
        isDisabled: true,
        toggle: true
      },

    ]
     

  }
   getAdminNavItems() {
    return [
      {
        navIcon: 'fa fa-tachometer',
        navLabel: 'Dashboard',
        navUrl: 'analytics',
        isDisabled: true,
        toggle: true
      }
      ,
      // {
      //   navIcon: 'fa fa-user-o',
      //   navLabel: 'Roles',
      //   navUrl: 'role',
      //   isDisabled: true,
      //   toggle: true
      // },
      //  {
      //    navIcon: 'fa fa-users',
      //    navLabel: 'Admin Staff',
      //    navUrl: 'adminusers',
      //    isDisabled: true,
      //    toggle: true
      // },

      // {
      //   navIcon: 'fa fa-superpowers',
      //   navLabel: 'Permissions',
      //   navUrl: 'permissions',
      //   isDisabled: true,
      //   toggle: true
      // },
      // {
      //   navIcon: 'fa fa-user-o',
      //   navLabel: 'Deposit Management',
      //   navUrl: 'adminDepositManagement',
      //   isDisabled: true,
      //   toggle: true
      // }
      // {
      //   navIcon: 'fa fa-superpowers',
      //   navLabel: 'Ticket Upload',
      //   navUrl: 'ticket-upload',
      //   isDisabled: true,
      //   toggle: true
      // },
      // {
      //   navIcon: 'fa fa-superpowers',
      //   navLabel: 'Banner Upload',
      //   navUrl: 'banner',
      //   isDisabled: true,
      //   toggle: true
      // },
     
      // {
      //   navIcon: 'fa fa-superpowers',
      //   navLabel: 'White Label',
      //   navUrl: 'whitelabel',
      //   isDisabled: true,
      //   toggle: true
      // },

      // {
      //   navIcon: 'fa fa-superpowers',
      //   navLabel: 'Credit Request',
      //   navUrl: 'creditsystem',
      //   isDisabled: true,
      //   toggle: true
      // },
      {
        navIcon: 'fa fa-superpowers',
        navLabel: 'Voucher',
        navUrl: 'voucher',
        isDisabled: true,
        toggle: true
      },
      // {
      //   navIcon: 'fa fa-users',
      //   navLabel: 'Distributor Management',
      //   navUrl: '',
      //   isDisabled: false,
      //   toggle: false,
      //   subList: [
      //     {
      //       subNavLabel: 'Create Distributor',
      //       subNavUrl: 'createDistributor'
      //     },
      //     {
      //       subNavLabel: 'Manage Distributor',
      //       subNavUrl: 'manageDistributor'
      //     }
      //   ]
      // }
      // ,

      {
        navIcon: 'fa fa-users',
        navLabel: 'B2B User Management',
        navUrl: '',
        isDisabled: false,
        toggle: false,
        subList: [
          {
            subNavLabel: 'Create Agent',
            subNavUrl: 'create-agent'
          },
          {
            subNavLabel: 'Manage Agent',
            subNavUrl: 'manage-agent'
          }
        ]
      },
      {
        navIcon: 'fa fa-superpowers',
        navLabel: 'B2B Markup Manager',
        navUrl: '',
        isDisabled: false,
        toggle: false,
        subList: [
          {
            subNavLabel: 'Hotel Markup Manager',
            subNavUrl: 'hotel-markup-manager'
          },
          {
            subNavLabel: 'Flight Markup Manager',
            subNavUrl: 'flight-markup-manager'
          }
        ]
      },
      {
        navIcon: 'fa fa-thumb-tack',
        navLabel: 'B2B Reports Manager',
        navUrl: '',
        isDisabled: false,
        toggle: false,
        subList: [
          {
            subNavLabel: 'Hotels Report',
            navUrl: 'hotel-report'
          },
          {
            subNavLabel: 'Flights Report',
            navUrl: 'flight-report'
          }
        ]
      }
      ,
      {
        navIcon: 'fa fa-users',
        navLabel: 'B2C User Management',
        navUrl: '',
        isDisabled: false,
        toggle: false,
        subList: [
          // {
          //   subNavLabel: 'Create User',
          //   subNavUrl: 'createUser'
          // },
          {
            subNavLabel: 'Manage User',
            subNavUrl: 'manage-user'
          }
        ]
      },
      {
        navIcon: 'fa fa-superpowers',
        navLabel: 'B2C Markup Manager',
        navUrl: '',
        isDisabled: false,
        toggle: false,
        subList: [
          {
            subNavLabel: 'Hotel Markup Manager',
            subNavUrl: 'hotel-user-markup-manager'
          },
          {
            subNavLabel: 'Flight Markup Manager',
            subNavUrl: 'flight-user-markup-manager'
          }
        ]
      },
      {
        navIcon: 'fa fa-thumb-tack',
        navLabel: 'B2C Reports Manager',
        navUrl: '',
        isDisabled: false,
        toggle: false,
        subList: [
          {
            subNavLabel: 'Hotels Report',
            subNavUrl: 'hotel-user-report'
          },
          {
            subNavLabel: 'Flights Report',
            subNavUrl: 'flight-user-report'
          }
        ]
      },
      // {
      //   navIcon: 'fa fa-thumb-tack',
      //   navLabel: 'Finance',
      //   navUrl: '',
      //   isDisabled: false,
      //   toggle: false,
      //   subList: [
      //     {
      //       subNavLabel: 'Agent Invoice Details',
      //       subNavUrl: 'agentInvoiceDetails'
      //     },
      //     {
      //       subNavLabel: 'Distributor Invoice Details',
      //       subNavUrl: 'distributorInvoiceDetails'
      //     },
      //    /* {
      //       subNavLabel: 'Flight Credit Note',
      //       subNavUrl: 'flightCreditNote'
      //     },
      //     {
      //       subNavLabel: 'Hotel Credit Note',
      //       subNavUrl: 'hotelCreditNote'
      //     },*/
      //     {
      //       subNavLabel: 'Customer Sales Report',
      //       subNavUrl: 'customerSalesReport'
      //     }
      //   ]
      // },

      {
        navIcon: 'fa fa-thumb-tack',
        navLabel: 'Report',
        navUrl: '',
        isDisabled: false,
        toggle: false,
        subList: [
          // {
          //   subNavLabel: 'Ledger Statement',
          //   subNavUrl: 'ledgerStatement'
          // },
          // {
          //   subNavLabel: 'Sales Report',
          //   subNavUrl: 'sales-report'
          // },
          // {
          //   subNavLabel: 'Show Invoices',
          //   subNavUrl: 'showInvoices'
          // },
          // {
          //   subNavLabel: 'Payment Collection',
          //   subNavUrl: 'paymentCollection'
          // },
          // {
          //   subNavLabel: 'Payment Cancellations',
          //   subNavUrl: 'paymentCancellation'
          // },
          {
            subNavLabel: 'Booking Report',
            subNavUrl: 'booking-report'
          },
          {
            subNavLabel: 'Pending Booking',
            subNavUrl: 'pending-booking'
          },
          {
            subNavLabel: 'Refund Report',
            subNavUrl: 'refund-report'
          },
          {
            subNavLabel: 'Cancellation Report',
            subNavUrl: 'cancellation-report'
          }
        ]
      }
    ];
  }

  getAgentNavItems() {
    return [
      {
        navIcon: 'fa fa-tachometer',
        navLabel: 'Dashboard',
        navUrl: 'agent-dashboard',
        isDisabled: true,
        toggle: true
      },
      {
        navIcon: 'fa fa-tachometer',
        navLabel: 'My Profile',
        navUrl:'agentprofile',
        isDisabled: true,
        toggle: true
      },
      {
        navIcon: 'fa fa-tachometer',
        navLabel: 'Hotel Bookings',
        navUrl:'agent-hotels',
        isDisabled: true,
        toggle: true
      }
      ,
      {
        navIcon: 'fa fa-tachometer',
        navLabel: 'Flight Bookings',
        navUrl:'agent-flights',
        isDisabled: true,
        toggle: true
      },
      // {
      //   navIcon: 'fa fa-user-o',
      //   navLabel: 'My Bookings',
      //   navUrl: '',
      //   isDisabled: false,
      //   toggle: false,
      //   subList: [
      //     {
      //       subNavLabel: 'Flights',
      //       subNavUrl: 'agent-flights'
      //     },
      //     {
      //       subNavLabel: 'Hotels',
      //       subNavUrl:'agent-hotels'
      //     }
      //   ]
      // },
      {
        navIcon: 'fa fa-users',
        navLabel: 'Flight Markup',
        navUrl: 'flightmarkup',
        isDisabled: true,
        toggle: true
      },

      {
        navIcon: 'fa fa-users',
        navLabel: 'Hotel Markup',
        navUrl: 'hotelmarkup',
        isDisabled: true,
        toggle: true
      },

      // {
      //   navIcon: 'fa fa-users',
      //   navLabel: 'Deposit Management',
      //   navUrl: 'agentDepositManagement',
      //   isDisabled: true,
      //   toggle: true
      // },

     
    ];
  }

  getImagesObject() {
    return [{
      image: '../../assets/images/beach.png',
      thumbImage: '../../assets/images/beach.png',
      alt: 'alt of image',
      title: 'Up to INR 10,000 Instant Discount* on GoAir Flight...'
    }, {
        image: '../../assets/images/1_holiday.jpg',
        thumbImage: '../../assets/images/1_holiday.jpg',
        title: 'Up to INR 1,800 instant discount on Domestic Hotel...',
        alt: 'Image alt'
    }, {
      image: '../../assets/images/girl.png',
      thumbImage: '../../assets/images/girl.png',
      alt: 'alt of image',
      title: 'title of image'
    }, {
      image: '../../assets/images/beach.png',
      thumbImage: '../../assets/images/beach.png',
      title: 'Image title',
      alt: 'Image alt'
    }];
  }
  CarCities(){
    return [
    { cityname: 'Agra'},
{cityname:'Ahmednagar'},
{cityname:'Ahmedabad'},
{cityname:'Ajmer'},
{cityname:'Akola'},
{cityname:'Alibaug'},
{cityname:'Allahabad'},
{cityname:'Alleppey'},
{cityname:'Alwar'},
{cityname:'Ambala'},
{cityname:'Amritsar'},
{cityname:'Asansol'},
{cityname:'Aurangabad'},
{cityname:'Badami'},
{cityname:'Bangalore'},
{cityname:'Baramati'},
{cityname:'Bareilly'},
{cityname:'Bhopal'},
{cityname:'Bhubaneshwar'},
{cityname:'Bhusawal'},
{cityname:'Bijapur'},
{cityname:'Bikaner'},
{cityname:'Bodh Gaya'},
{cityname:'Chamba'},
{cityname:'Chandigarh'},
{cityname:'Chennai'},
{cityname:'Chikmagalur'},
{cityname:'Chiplun'},
{cityname:'Coimbatore'},
{cityname:'Coorg'},
{cityname:'Jim Corbett National Park'},
{cityname:'Dahanu'},
{cityname:'Dalhousie'},
{cityname:'Daman'},
{cityname:'Darjeeling'},
{cityname:'Dehradun'},
{cityname:'Delhi'},
{cityname:'Dharamsala'},
{cityname:'Dwarka'},
{cityname:'Gangtok'},
{cityname:'Ganpatipule'},
{cityname:'Goa'},
{cityname:'Gondia'},
{cityname:'Gorakhpur'},
{cityname:'Guntur'},
{cityname:'Gwalior'},
{cityname:'Hampi'},
{cityname:'Haridwar'},
{cityname:'Hassan'},
{cityname:'Hogenakkal'},
{cityname:'Hyderabad'},
{cityname:'Indore'},
{cityname:'Jabalpur'},
{cityname:'Jaipur'},
{cityname:'Jaisalmer'},
{cityname:'Jalandhar'},
{cityname:'Jalgaon'},
{cityname:'Jammu'},
{cityname:'Jamnagar'},
{cityname:'Jamshedpur'},
{cityname:'Jhansi'},
{cityname:'Jodhpur'},
{cityname:'Junagadh'},
{cityname:'Kalyan'},
{cityname:'Kanpur'},
{cityname:'Kanyakumari'},
{cityname:'Karwar'},
{cityname:'Kathgodam'},
{cityname:'Khajuraho'},
{cityname:'Kodaikanal'},
{cityname:'Kolhapur'},
{cityname:'Kolkata'},
{cityname:'Kovalam'},
{cityname:'Kozhikode'},
{cityname:'Kullu'},
{cityname:'Kurnool'},
{cityname:'Kurukshetra'},
{cityname:'Latur'},
{cityname:'Lonavala'},
{cityname:'Ludhiana'},
{cityname:'Lucknow'},
{cityname:'Madurai'},
{cityname:'Mahabaleshwar'},
{cityname:'Manali'},
{cityname:'Mangalore'},
{cityname:'Manmad'},
{cityname:'Matheran'},
{cityname:'Mathura'},
{cityname:'Mount Abu'},
{cityname:'Mumbai'},
{cityname:'Munnar'},
{cityname:'Mussoorie'},
{cityname:'Mysore'},
{cityname:'Nagapattinam'},
{cityname:'Nagpur'},
{cityname:'Nainital'},
{cityname:'Nanded'},
{cityname:'Nasik'},
{cityname:'Ooty'},
{cityname:'Orchha'},
{cityname:'Panchgani'},
{cityname:'Panipat'},
{cityname:'Parbhani'},
{cityname:'Patiala'},
{cityname:'Patna'},
{cityname:'Pollachi'},
{cityname:'Pondicherry'},
{cityname:'Porbandar'},
{cityname:'Pune'},
{cityname:'Puri'},
{cityname:'Raipur'},
{cityname:'Rameshwaram'},
{cityname:'Ranchi'},
{cityname:'Ranthambore'},
{cityname:'Rishikesh'},
{cityname:'Saharanpur'},
{cityname:'Satara'},
{cityname:'Shegaon'},
{cityname:'Shimla'},
{cityname:'Shirdi'},
{cityname:'Sringeri'},
{cityname:'Silvassa'},
{cityname:'Solapur'},
{cityname:'Somnath'},
{cityname:'Thanjavur'},
{cityname:'Thrissur'},
{cityname:'Trichy'},
{cityname:'Tirunelveli'},
{cityname:'Tirupati'},
{cityname:'Trivandrum'},
{cityname:'Udaipur'},
{cityname:'Udupi'},
{cityname:'Ujjain'},
{cityname:'Velankanni'},
{cityname:'Varanasi'},
{cityname:'Visakhapatnam'},
{cityname:'Warangal'},
{cityname:'Yavatmal'},
{cityname:'Agartala'},
{cityname:'Bilaspur'},
{cityname:'Guwahati'},
{cityname:'Imphal'},
{cityname:'Jorhat'},
{cityname:'Cochin'},
{cityname:'Kohima'},
{cityname:'Leh'},
{cityname:'Patnitop'},
{cityname:'Rourkela'},
{cityname:'Sabarimala'},
{cityname:'Srinagar'},
{cityname:'Tawang'},
{cityname:'Tezpur'},
{cityname:'Vijayawada'},
{cityname:'Vadodara'},
{cityname:'Surat'},
{cityname:'Rajkot'},
{cityname:'Palakkad'},
{cityname:'Noida'},
{cityname:'Kota'},
{cityname:'Hubli'},
{cityname:'Gurgaon'},
{cityname:'Gaya'},
{cityname:'Belgaum'},
{cityname:'Meerut'},
{cityname:'Katra'},
{cityname:'Itanagar'},
{cityname:'Dibrugarh'},
{cityname:'Dhule'},
{cityname:'Kopargaon'},
{cityname:'Nagarsol'},
{cityname:'Guna'},
{cityname:'Pathankot'},
{cityname:'Nizamabad'},
{cityname:'Sangli'},
{cityname:'Ratnagiri'},
{cityname:'Bhavnagar'},
{cityname:'Eluru'},
{cityname:'Bhuj'},
{cityname:'Kakinada'},
{cityname:'Hosur'},
{cityname:'Kannur'},

{cityname:'Dindigul'},
{cityname:'Bekal'},
{cityname:'Bhatkal'},
{cityname:'Pathanamthitta'},
{cityname:'Kollam'},
{cityname:'Tuticorin'},
{cityname:'Moga'},
{cityname:'Tabo'},
{cityname:'Bundi'},
{cityname:'Palanpur'},

{cityname:'Amravati'},
{cityname:'Bagdogra'},
{cityname:'Sambalpur'},
{cityname:'Srikakulam'},
{cityname:'Siliguri'},
{cityname:'Panchkula'},
{cityname:'Theni'},
{cityname:'Thane'},
{cityname:'Silchar'},
{cityname:'Shimoga'},
{cityname:'Bharuch'},
{cityname:'Kalka'},
{cityname:'Secunderabad'},
{cityname:'Bhatinda'},
{cityname:'Salem'},

{cityname:'Anand'},
{cityname:'Bhiwani'},
{cityname:'Bellary'},
{cityname:'Sonipat'},
{cityname:'Mohali'},
{cityname:'Rohtak'},
{cityname:'Diu'},
{cityname:'Satna'},
{cityname:'Chandrapur'},
{cityname:'Bidar'},
{cityname:'Dhanbad'},
{cityname:'Faridabad'},
{cityname:'Jharsuguda'},
{cityname:'Vellore'},
{cityname:'Sikar'},
{cityname:'Hisar'},
{cityname:'Moradabad'},
{cityname:'Kharagpur'},
{cityname:'Port Blair'},
{cityname:'Khandwa'},
{cityname:'Bathinda'},
{cityname:'Ghaziabad'},
{cityname:'Kothagudem'},
{cityname:'Abohar'},
{cityname:'Arakkonam'},
{cityname:'Bargarh'},
{cityname:'Chengalpattu'},
{cityname:'Daund Railway Station'},
{cityname:'Deoria Sadar'},
{cityname:'Ernakulam'},
{cityname:'Gulbarga'},
{cityname:'Hospet'},
{cityname:'Karaikal'},

{cityname:'Londa Junction'},
{cityname:'Marwar Jn'},
{cityname:'Mughal Sarai Jn'},
{cityname:'Nabha'},
{cityname:'Nagercoil'},
{cityname:'Navi Mumbai'},
{cityname:'North Lakhimpur'},
{cityname:'Pantnagar'},
{cityname:'Phagwara'},
{cityname:'Rewari'},
{cityname:'Tadepalligudem'},
{cityname:'Vapi'},
{cityname:'Vasai'},
{cityname:'Villupuram'},
{cityname:'Barbil'},
{cityname:'Rajahmundry'},
{cityname:'Firozpur'},
{cityname:'Bhiwadi'},
{cityname:'Kapurthala'},
{cityname:'Ramagundam'},
{cityname:'Baltal'}

    ]
  }

  originDestinations() {
    return [
      {
        AirportCode: 'DEL',
        AirportName: 'Indira Gandhi Airport',
        CityName: 'Delhi',
        CountryName: 'India'
      },
      {
        AirportCode: 'BOM',
        AirportName: 'Mumbai',
        CityName: 'Mumbai',
        CountryName: 'India'
      },
      {
        AirportCode: 'BLR',
        AirportName: 'Bengaluru Intl',
        CityName: 'Bangalore',
        CountryName: 'India'
      },
      {
        AirportCode: 'GOI',
        AirportName: 'Dabolim',
        CityName: 'Goa',
        CountryName: 'India'
      },
      {
        AirportCode: 'MAA',
        AirportName: 'Chennai',
        CityName: 'Chennai',
        CountryName: 'India'
      },
      {
        AirportCode: 'CCU',
        AirportName: 'Netaji Subhash Chandra Bose International Airport',
        CityName: 'Kolkata',
        CountryName: 'India'
      },
      {
        AirportCode: 'HYD',
        AirportName: 'Shamsabad International Airport',
        CityName: 'Hyderabad',
        CountryName: 'India'
      },
      {
        AirportCode: 'PNQ',
        AirportName: 'Lohegaon',
        CityName: 'Pune',
        CountryName: 'India'
      },
      {
        AirportCode: 'AMD',
        AirportName: 'Ahmedabad',
        CityName: 'Ahmedabad',
        CountryName: 'India'
      },
      {
        AirportCode: 'COK',
        AirportName: 'Kochi',
        CityName: 'Kochi',
        CountryName: 'India'
      },
      {
        AirportCode: 'JAI',
        AirportName: 'Jaipur',
        CityName: 'Jaipur',
        CountryName: 'India'
      },
      {
        AirportCode: 'DXB',
        AirportName: 'Dubai',
        CityName: 'Dubai',
        CountryName: 'United Arab Emirates'
      },
      {
        AirportCode: 'SIN',
        AirportName: 'Changi',
        CityName: 'Singapore',
        CountryName: 'Singapore'
      },
      {
        AirportCode: 'BKK',
        AirportName: 'Bangkok Int\u0027l',
        CityName: 'Bangkok',
        CountryName: 'Thailand'
      },
      {
        AirportCode: 'KUL',
        AirportName: 'Kuala Lum Intl.',
        CityName: 'Kualalumpur',
        CountryName: 'Malaysia'
      },
      {
        AirportCode: 'HKG',
        AirportName: 'Hong Kong Int\u0027l',
        CityName: 'Hong Kong',
        CountryName: 'Hong Kong'
      },
      {
        AirportCode: 'DOH',
        AirportName: 'Doha',
        CityName: 'Doha',
        CountryName: 'Qatar'
      },
      {
        AirportCode: 'CMB',
        AirportName: 'Katunayake',
        CityName: 'Colombo',
        CountryName: 'Sri Lanka'
      }
    ];
  }


  getInnoDemos() {
    return [{
      image: '../../assets/images/1_holiday.jpg',
      title: 'Colombo',
      description: 'Explore Colombo in Under Rs. 25,000'
    }, {
      image: '../../assets/images/2_holiday.jpg',
      title: 'Holidays',
      description: 'Holidays Under 10K This February'
    }, {
      image: '../../assets/images/3_holiday.jpg',
      title: 'Visa',
      description: 'List of Countries Offering Visa on Arrival'
    }, {
      image: '../../assets/images/4_holiday.jpg',
      title: 'Destinations',
      description: 'Top 6 Most Romantic Honeymoon Destinations'
    }];
  }
  getFooterObj() {
    return [{
      title: 'Tripee',
      content: ['Careers', 'News', 'Policies', 'Diversity & Belonging', 'Accessibility']
    }, {
      title: 'Discover',
      content: ['Trust & Safety', 'Travel Credit', 'Tripee Citizen', 'Business Travel', 'Things To Do']
    }, {
      title: 'Hosting',
      content: ['Why Host', 'Hospitality', 'Responsible Hosting', 'Community Centre', 'Host an Experience', 'Open Homes']
    }, {
      title: 'Support',
      content: ['Help']
    }];
  }
}
