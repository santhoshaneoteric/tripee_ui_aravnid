import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  constructor() { }
  private subject = new Subject<any>();
  saveUserInfo(obj: object) {
    this.subject.next(obj);
  }
  getUserInfo(): Observable<any> {
      return this.subject.asObservable();
  }
}
