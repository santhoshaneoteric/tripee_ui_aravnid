import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './landing/landing.component';
import { FlightResultsComponent } from './+flight/flight-results/flight-results.component';
import { FlightDetailsComponent } from './+flight/flight-details/flight-details.component';
import { RoundDialogComponent } from './+flight/round-dialog/round-dialog.component';

import { AgentaccountComponent } from 'src/app/agentaccount/agentaccount.component';
import { AgentsignupComponent } from './agentsignup/agentsignup.component';
//import { AgentcreditComponent } from './agent/agentcredit/agentcredit.component';
import { MytripsComponent } from 'src/app/mytrips/mytrips.component';
import { MytravellerComponent } from 'src/app/mytraveller/mytraveller.component';
import { MycouponsComponent } from 'src/app/mycoupons/mycoupons.component';
import { MyprofileComponent } from 'src/app/myprofile/myprofile.component';
import { VerifyuseraccountComponent } from './verifyuseraccount/verifyuseraccount.component';
import { ChangePasswordComponent } from 'src/app/change-password/change-password.component'
import { ResetpasswordComponent } from './resetpassword/resetpassword.component'

import { AgentDashboardComponent } from './agent/agent-dashboard/agent-dashboard.component';
import { AgentFlightsComponent } from 'src/app/agent/agent-dashboard/agent-flights/agent-flights.component';
import { AgentHotelsComponent } from 'src/app/agent/agent-dashboard/agent-hotels/agent-hotels.component';
//import { AgentMarkupManagementComponent } from './agent/agent-markup-management/agent-markup-management.component';
import { AgentprofileComponent } from 'src/app/agent/agent-dashboard/agentprofile/agentprofile.component';
import { FlightmarkupComponent } from 'src/app/agent/agent-dashboard/flightmarkup/flightmarkup.component';
import { HotelmarkupComponent } from 'src/app/agent/agent-dashboard/hotelmarkup/hotelmarkup.component';

import { AdmindashboardComponent } from './admin/admin-dashboard/admindashboard/admindashboard.component';
import { CreateAgentComponent } from 'src/app/admin/admin-dashboard/admindashboard/b2b-user-management/create-agent/create-agent.component';
import { ManageAgentComponent } from 'src/app/admin/admin-dashboard/admindashboard/b2b-user-management/manage-agent/manage-agent.component';
import { AdminLoginComponent } from './admin/admin-login/admin-login.component';
import { HotelMarkupManagerComponent } from 'src/app/admin/admin-dashboard/admindashboard/b2b-markup-manager/hotel-markup-manager/hotel-markup-manager.component';
// tslint:disable-next-line: max-line-length
import { FlightMarkupManagerComponent } from 'src/app/admin/admin-dashboard/admindashboard/b2b-markup-manager/flight-markup-manager/flight-markup-manager.component';

import { HotelReportComponent } from 'src/app/admin/admin-dashboard/admindashboard/b2b-report-manager/hotel-report/hotel-report.component';
import { FlightReportComponent } from 'src/app/admin/admin-dashboard/admindashboard/b2b-report-manager/flight-report/flight-report.component';
import { HotelUserMarkupManagerComponent } from 'src/app/admin/admin-dashboard/admindashboard/b2c-markup-manager/hotel-user-markup-manager/hotel-user-markup-manager.component';

import { FlightUserMarkupManagerComponent } from 'src/app/admin/admin-dashboard/admindashboard/b2c-markup-manager/flight-user-markup-manager/flight-user-markup-manager.component';
import { HotelUserReportComponent } from 'src/app/admin/admin-dashboard/admindashboard/b2c-report-manager/hotel-user-report/hotel-user-report.component';
import { FlightUserReportComponent } from 'src/app/admin/admin-dashboard/admindashboard/b2c-report-manager/flight-user-report/flight-user-report.component';
import { ManageUserComponent } from 'src/app/admin/admin-dashboard/admindashboard/b2c-user-management/manage-user/manage-user.component';
import { VoucherComponent } from 'src/app/admin/admin-dashboard/admindashboard/voucher/voucher.component';

import { SalesReportComponent } from 'src/app/admin/admin-dashboard/admindashboard/reports/sales-report/sales-report.component';

import { PaymentCancellationComponent } from 'src/app/admin/admin-dashboard/admindashboard/reports/payment-cancellation/payment-cancellation.component';
import { BookingReportComponent } from 'src/app/admin/admin-dashboard/admindashboard/reports/booking-report/booking-report.component';
import { PendingBookingComponent } from 'src/app/admin/admin-dashboard/admindashboard/reports/pending-booking/pending-booking.component';
import { RefundReportComponent } from 'src/app/admin/admin-dashboard/admindashboard/reports/refund-report/refund-report.component';
import { CancellationReportComponent } from 'src/app/admin/admin-dashboard/admindashboard/reports/cancellation-report/cancellation-report.component';

import { CarresultsComponent } from './Cars/carresults/carresults.component';
import { FlightReviewComponent } from './+flight/flight-review/flight-review.component';
import { RoundflightreviewComponent } from './+flight/roundflightreview/roundflightreview.component'
import { RoundtripFlightdetailsComponent } from './+flight/roundtrip-flightdetails/roundtrip-flightdetails.component'



 

const routes: Routes = [
  { path: '',
redirectTo: '/home',
pathMatch: 'full'
}, {
path: 'home',
component: LandingComponent
}, {
  path: 'search-results/:id',
  component: FlightResultsComponent
}, {
  path: 'flight-details',
  component: FlightDetailsComponent
},
{
  path:'flight-review',
  component:FlightReviewComponent
},
{
  path:'roundtrip-flightdetails',
  component:RoundtripFlightdetailsComponent
},
{
  path:'roundflightreview',
  component:RoundflightreviewComponent
},
{
  path:'carresults',
  component:CarresultsComponent

},

{
  path:'agentaccount',
  component:AgentaccountComponent

},
{
  path:'agentsignup',
  component:AgentsignupComponent
},
{
  path:'resetpassword',
  component:ResetpasswordComponent
  
},
{
  path:'verifyuseraccount',
  component:VerifyuseraccountComponent 

},
{
  path:'change-password',
  component:ChangePasswordComponent
}
,
   {
      path:'myprofile',
      component:MyprofileComponent
    },
    {
      path:'mytrips',
      component:MytripsComponent
    },
    {
      path:'mytraveller',
      component:MytravellerComponent
    },
    {
      path:'mycoupons',
      component:MycouponsComponent 
    }
  
,
{
  path: 'admin',
  component: AdminLoginComponent,
 // canActivate: [AuthGuard]
},
 
{
  path:'admindashboard',
  component:AdmindashboardComponent,
  children:[
    
    {
      path:'create-agent',
      component:CreateAgentComponent
    },

    {
      path:'create-agent/:id',
      component:CreateAgentComponent
    },
    {
      path:'manage-agent',
      component:ManageAgentComponent 
    },
    {
      path:'hotel-markup-manager',
      component:HotelMarkupManagerComponent 
    },
    {
      path:'flight-markup-manager',
      component:FlightMarkupManagerComponent 
    },
    {
      path:'flight-report',
      component:HotelReportComponent
    },
    {
      path:'hotel-report',
      component:FlightReportComponent
    },
    {
      path:'flight-user-markup-manager',
      component:HotelUserMarkupManagerComponent
    },
    {
      path:'hotel-user-markup-manager',
      component:FlightUserMarkupManagerComponent
    },
    {
      path:'manage-user',
      component:ManageUserComponent
    },
    {
      path:'flight-user-report',
      component:FlightUserReportComponent
    },
    {
      path:'hotel-user-report',
      component:HotelUserReportComponent
    },
    {
      path:'voucher',
      component:VoucherComponent 
    },
    {
      path:'cancellation-report',
      component:CancellationReportComponent 
    },
    {
      path:'sales-report',
      component:SalesReportComponent
    },
    {
      path:'booking-report',
      component:BookingReportComponent 
    },
    {
      path:'pending-booking',
      component:PendingBookingComponent 
    },
    {
      path:'refund-report',
      component:RefundReportComponent 
    },
   

  ]
}

,

 {
  path: 'agentdashboard',
  component: AgentDashboardComponent,
  //canActivate: [AuthGuard],
  children: [
    {
      path:'agentprofile',
      component:AgentprofileComponent
    },
    {
    path: 'agent-flights',
    component: AgentFlightsComponent
  }, {
    path: 'agent-hotels',
    component: AgentHotelsComponent
  },
  {
    path:'flightmarkup',
    component:FlightmarkupComponent

  },

  {
    path:'hotelmarkup',
    component:HotelmarkupComponent
  }
  // {
  //   path: 'flightmarkup',
  //   component: FlightmarkupComponent 
  // },
  // {
  //   path: 'hotelmarkup',
  //   component: HotelmarkupComponent 
  // },

  // {
  //   path: 'agentDepositManagement',
  //   component: AgentDepositManagementComponent
  // },
  //  {
  //   path: 'agentcredit',
  //   component: AgentcreditComponent
  // }
]
},


{
  path:'agentaccount',
  component:AgentaccountComponent

},
{
path:'agentsignup',
component:AgentsignupComponent
},
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
