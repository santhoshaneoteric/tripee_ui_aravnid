import { Component, OnInit } from '@angular/core';

import { parseISO, differenceInHours, differenceInMinutes, getHours } from 'date-fns';

import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-flight-review',
  templateUrl: './flight-review.component.html',
  styleUrls: ['./flight-review.component.scss']
})
export class FlightReviewComponent implements OnInit {
  showFields;
  reviewData;
  fromAirportCode;
  toAirportCode;
  adultCount;
  childCount;
  infantCount;
  airlineName;
  airlineCode;
  flightNo;
  flightClass;
  fromAirportName;
  toAirportName;
  depTime;
  arrTime;
  depDate;
  arrDate;
  fromTerminal;
  toTerminal;
  adultFare;
  childFare;
  infantFare;
  fuelCharges;
  cgst;
  sgst;
  cuteFee;
  totalAmount;
  remainingTime
  constructor(private router: Router, private activeRoute: ActivatedRoute,) { }

  ngOnInit() {
    // if (localStorage.getItem('flightDataType') === 'tripJack') {
      this.showFields = true;
      this.reviewData = JSON.parse(localStorage.getItem('singleBookFlightData'));
      console.log(this.reviewData);
      this.fromAirportCode = this.reviewData.searchQuery.routeInfos[0].fromCityOrAirport.code;
      this.toAirportCode = this.reviewData.searchQuery.routeInfos[0].toCityOrAirport.code;
      this.adultCount = this.reviewData.searchQuery.paxInfo.ADULT;
      this.childCount = this.reviewData.searchQuery.paxInfo.CHILD;
      this.infantCount = this.reviewData.searchQuery.paxInfo.INFANT;
      this.airlineName = this.reviewData.tripInfos[0].sI[0].fD.aI.name;
      this.airlineCode = this.reviewData.tripInfos[0].sI[0].fD.aI.code;
      this.flightNo = this.reviewData.tripInfos[0].sI[0].fD.fN;
      this.flightClass = this.reviewData.searchQuery.cabinClass;
      this.fromAirportName = this.reviewData.searchQuery.routeInfos[0].fromCityOrAirport.name;
      this.toAirportName = this.reviewData.searchQuery.routeInfos[0].toCityOrAirport.name;
      this.depTime = this.reviewData.tripInfos[0].sI[0].dt.split('T')[1];
      this.arrTime = this.reviewData.tripInfos[0].sI[0].at.split('T')[1];
      this.depDate = this.reviewData.tripInfos[0].sI[0].dt.split('T')[0];
      this.arrDate = this.reviewData.tripInfos[0].sI[0].at.split('T')[0];
      this.fromTerminal = this.reviewData.tripInfos[0].sI[0].da.terminal;
      this.toTerminal = this.reviewData.tripInfos[0].sI[0].aa.terminal;
      this.totalAmount = this.reviewData.totalPriceInfo.totalFareDetail.fC.TF;
      const diffInHours = differenceInHours(parseISO(this.reviewData.tripInfos[0].sI[0].at), parseISO(this.reviewData.tripInfos[0].sI[0].dt));
      const diffInMinutes = differenceInMinutes(parseISO(this.reviewData.tripInfos[0].sI[0].at), parseISO(this.reviewData.tripInfos[0].sI[0].dt)) % 60;
        this.remainingTime = diffInHours + 'hrs ' + diffInMinutes + 'mins';
      console.log(this.remainingTime)
      
    }
    OnContinue(){
      this.router.navigateByUrl('flight-details');
    }
    

}


