import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoundtripFlightdetailsComponent } from './roundtrip-flightdetails.component';

describe('RoundtripFlightdetailsComponent', () => {
  let component: RoundtripFlightdetailsComponent;
  let fixture: ComponentFixture<RoundtripFlightdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoundtripFlightdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoundtripFlightdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
