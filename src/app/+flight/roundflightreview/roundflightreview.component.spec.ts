import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoundflightreviewComponent } from './roundflightreview.component';

describe('RoundflightreviewComponent', () => {
  let component: RoundflightreviewComponent;
  let fixture: ComponentFixture<RoundflightreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoundflightreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoundflightreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
