import { Injectable } from '@angular/core';
import { pipe, groupBy, prop, map, pluck, flatten, uniq, toPairs, zip, zipObj } from 'ramda';
import { parseISO, differenceInHours, differenceInMinutes } from 'date-fns';


@Injectable({
  providedIn: 'root'
})
export class FlightresulthelperService {

  
  constructor() { }

  searchParsedResponse(mergedSingleTripArr, airLineNameBasedOnCode, airPortNameBasedOnFromCode,
                       airPortNameBasedOnToCode, tempResultsArr, resultsArr) {
    const tripJackData = JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos.ONWARD;
    console.log("came to helper service")
    //console.log(tripJackData)
    console.log("aftre trip jack data")
  
   
    tripJackData.forEach(element => {
        const tripObj = {
            airlineName: element.sI[0].fD.aI.name,
            airlineCode: element.sI[0].fD.aI.code,
            departureTime: element.sI[0].dt,
            departureCity: element.sI[0].da.city,
            arrivalTime: element.sI[0].at,
            aarivalCity: element.sI[0].aa.city,
            flightNumber: element.sI[0].fD.fN,
            totalFare: element.totalPriceList[0].fd.ADULT.fC.TF,
           // FareId:element.totalPriceList[0].fd.id
          
        };
        //console.log(tripObj)
        mergedSingleTripArr.push(tripObj);
        
       
    });
        tempResultsArr = pipe(
        groupBy(prop('flightNumber')),
        map(pluck('totalFare')),
        map(flatten),
        map(uniq),
        toPairs,
        map(zipObj(['flightNumber', 'totalFare']))
    )(mergedSingleTripArr);
    mergedSingleTripArr.forEach(item => {
      tempResultsArr.forEach(subItem => {
        if (item.flightNumber === subItem.flightNumber) {
          subItem.flightNumber = subItem.flightNumber,
          subItem.airlineName = item.airlineName,
          subItem.airlineCode = item.airlineCode,
          subItem.departureTime = item.departureTime,
          subItem.departureCity = item.departureCity,
          subItem.arrivalTime = item.arrivalTime,
          subItem.aarivalCity = item.aarivalCity,
          subItem.totalFare = subItem.totalFare;
        }
      });
    });
    resultsArr = tempResultsArr;
    resultsArr.forEach(element => {
        const diffInHours = differenceInHours(parseISO(element.arrivalTime), parseISO(element.departureTime));
        const diffInMinutes = differenceInMinutes(parseISO(element.arrivalTime), parseISO(element.departureTime)) % 60;
        element.remainingTime = diffInHours + 'hrs ' + diffInMinutes + 'mins';

    });
    return resultsArr;
  }
  searchParsedMultiCityResponse(tripJackMultiCity, tripJackMutliObj) {
    const tripJackData = JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos;
   
  }

  searchParsedRoundTripResponse(airLineNameBasedOnCode, airPortNameBasedOnFromCode,
                                airPortNameBasedOnToCode) {
    const completeRoundTripObj: any = {};
    const onwardBothTripArr = [];
    const towardsBothTripArr = [];
    let tempOneResultsArr = [];
    let tempTwoResultsArr = [];
    const fromCode = localStorage.getItem('roundFromAirportCode');
    const toCode = localStorage.getItem('roundToAirportCode');
   // const towardsTripJackObj=JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos.ONWARD;
    const tripJackData = JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos.COMBO;
   
    tripJackData.forEach(element => {
      if (element.sI[0].da.code === fromCode && element.sI[0].aa.code === toCode) {
        const onwardTripJackObj = {
          airlineName: element.sI[0].fD.aI.name,
          airlineCode: element.sI[0].fD.aI.code,
          departureTime: element.sI[0].dt,
          departureCity: element.sI[0].da.city,
          arrivalTime: element.sI[0].at,
          aarivalCity: element.sI[0].aa.city,
          flightNumber: element.sI[0].fD.fN,
          totalFare: element.totalPriceList[0].fd.ADULT.fC.TF
        };
        onwardBothTripArr.push(onwardTripJackObj);
      }
      if (element.sI[0].da.code === toCode && element.sI[0].aa.code === fromCode) {
        const towardsTripJackObj = {
          airlineName: element.sI[0].fD.aI.name,
          airlineCode: element.sI[0].fD.aI.code,
          departureTime: element.sI[0].dt,
          departureCity: element.sI[0].da.city,
          arrivalTime: element.sI[0].at,
          aarivalCity: element.sI[0].aa.city,
          flightNumber: element.sI[0].fD.fN,
          totalFare: element.totalPriceList[0].fd.ADULT.fC.TF
        };
        towardsBothTripArr.push(towardsTripJackObj);
      }
         });
    
    tempOneResultsArr = pipe(
      groupBy(prop('flightNumber')),
      map(pluck('totalFare')),
      map(flatten),
      map(uniq),
      toPairs,
      map(zipObj(['flightNumber', 'totalFare']))
      )(onwardBothTripArr);
    onwardBothTripArr.forEach(item => {
      tempOneResultsArr.forEach(subItem => {
        if (item.flightNumber === subItem.flightNumber) {
          subItem.flightNumber = subItem.flightNumber,
          subItem.airlineName = item.airlineName,
          subItem.airlineCode = item.airlineCode,
          subItem.departureTime = item.departureTime,
          subItem.departureCity = item.departureCity,
          subItem.arrivalTime = item.arrivalTime,
          subItem.aarivalCity = item.aarivalCity,
          subItem.totalFare = subItem.totalFare;
        }
      });
    });
    tempTwoResultsArr = pipe(
      groupBy(prop('flightNumber')),
      map(pluck('totalFare')),
      map(flatten),
      map(uniq),
      toPairs,
      map(zipObj(['flightNumber', 'totalFare']))
      )(towardsBothTripArr);
    towardsBothTripArr.forEach(item => {
      tempTwoResultsArr.forEach(subItem => {
        if (item.flightNumber === subItem.flightNumber) {
          subItem.flightNumber = subItem.flightNumber,
          subItem.airlineName = item.airlineName,
          subItem.airlineCode = item.airlineCode,
          subItem.departureTime = item.departureTime,
          subItem.departureCity = item.departureCity,
          subItem.arrivalTime = item.arrivalTime,
          subItem.aarivalCity = item.aarivalCity,
          subItem.totalFare = subItem.totalFare;
        }
      });
    });
    tempOneResultsArr.forEach(element => {
        const diffInHours = differenceInHours(parseISO(element.arrivalTime), parseISO(element.departureTime));
        const diffInMinutes = differenceInMinutes(parseISO(element.arrivalTime), parseISO(element.departureTime)) % 60;
        element.remainingTime = diffInHours + 'hrs ' + diffInMinutes + 'mins';
    });
    tempTwoResultsArr.forEach(element => {
      const diffInHours = differenceInHours(parseISO(element.arrivalTime), parseISO(element.departureTime));
      const diffInMinutes = differenceInMinutes(parseISO(element.arrivalTime), parseISO(element.departureTime)) % 60;
      element.remainingTime = diffInHours + 'hrs ' + diffInMinutes + 'mins';
    });
    completeRoundTripObj.onWardArr = tempOneResultsArr;
    completeRoundTripObj.returnArr = tempTwoResultsArr;
    return completeRoundTripObj;
  } 


}
