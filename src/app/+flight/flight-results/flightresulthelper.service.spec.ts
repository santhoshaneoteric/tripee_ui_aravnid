import { TestBed } from '@angular/core/testing';

import { FlightresulthelperService } from './flightresulthelper.service';

describe('FlightresulthelperService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FlightresulthelperService = TestBed.get(FlightresulthelperService);
    expect(service).toBeTruthy();
  });
});
