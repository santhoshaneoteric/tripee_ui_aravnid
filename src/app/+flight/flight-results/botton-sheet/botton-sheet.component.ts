import { Component, OnInit, Inject } from '@angular/core';
import {MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {MAT_BOTTOM_SHEET_DATA} from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import { NgxSpinnerService } from 'ngx-spinner';
import { FlightsearchService } from 'src/app/+flight/flight-results/flightsearch.service';

@Component({
  selector: 'app-botton-sheet',
  templateUrl: './botton-sheet.component.html',
  styleUrls: ['./botton-sheet.component.scss'],
  providers: [FlightsearchService],
})
export class BottonSheetComponent implements OnInit {
leftData: any;
rightData: any;
math = Math;
  constructor(private router: Router,private spinner: NgxSpinnerService , private activeRoute: ActivatedRoute, private flightSearchDetailsService:FlightsearchService, private _bottomSheetRef: MatBottomSheetRef<BottonSheetComponent>,
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: any) {}

  ngOnInit() {
    console.log(this.data);
    this.leftData = this.data.leftData;
    this.rightData = this.data.rightData;
    
  }
  openLink(event: MouseEvent): void {
    this._bottomSheetRef.dismiss();
    event.preventDefault();
  }

 

  onRoundBook() {
    let leftfare=localStorage.getItem('LeftFareId');
    let rightfare=localStorage.getItem('rightFared');
    
      const sendObject = {
        priceIds: [leftfare,rightfare]
      };
      
      console.log(sendObject);
      this.spinner.show();
      this.flightSearchDetailsService.flightReview(sendObject).subscribe(data => {
        this.spinner.hide();
        console.log(data)
        let sing=localStorage.setItem("singleBookFlightData", JSON.stringify(data));
        let singlebook=localStorage.getItem('singleBookFlightData')
        console.log(singlebook)
        
        this.router.navigateByUrl('roundflightreview');
      });
    
    // localStorage.setItem('leftSelectedData', JSON.stringify(this.leftData));
    // localStorage.setItem('rightSelectedData', JSON.stringify(this.rightData));
    
    
    this.router.navigateByUrl('flight-review');
  }

}
