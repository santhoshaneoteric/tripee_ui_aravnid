import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Constants } from 'src/app/common/constants.enum';

@Injectable({
  providedIn: 'root'
})
export class FlightsearchService {

  constructor(private http: HttpClient) { }

  fareRules(obj): Observable<any>  {
    return this.http.post(Constants.API_URL + 'flightsearch/farerule', obj, {})
      .pipe(map((res: any) => res));
  }

  flightReview(obj): Observable<any>  {
    return this.http.post(Constants.API_URL + 'flightsearch/review', obj, {})
      .pipe(map((res: any) => res));
  }
}
