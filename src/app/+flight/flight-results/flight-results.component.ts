import { Component, OnInit } from '@angular/core';
import { Options } from 'ng5-slider';
import { parseISO, differenceInHours, differenceInMinutes, getHours } from 'date-fns';
import { Router, ActivatedRoute } from '@angular/router';
import { format } from 'date-fns';
import { LandingService } from '../../landing/landing.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { HelperService } from '../../common/helper.service';

import { LandingComponent } from 'src/app/landing/landing.component'

import { LoginComponent } from 'src/app/login/login.component';
import { FlightresulthelperService } from './flightresulthelper.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RoundDialogComponent } from 'src/app/+flight/round-dialog/round-dialog.component';
import { FlightsearchService } from 'src/app/+flight/flight-results/flightsearch.service';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { BottonSheetComponent } from './botton-sheet/botton-sheet.component';
import { orderBy } from 'lodash';
import { JwtHelperService } from '@auth0/angular-jwt';
import { SearchcomponentComponent } from 'src/app/searchcomponent/searchcomponent.component'
import { pipe, groupBy, prop, map, pluck, flatten, uniq, toPairs, zip, zipObj, sort, comparator, lt } from 'ramda';

@Component({
    selector: 'app-flight-results',
    templateUrl: './flight-results.component.html',
    styleUrls: ['./flight-results.component.scss'],
    providers: [FlightresulthelperService, LandingService, FlightsearchService],
    entryComponents: [RoundDialogComponent]
})
export class FlightResultsComponent implements OnInit {
    showReturnFlightDate = true;

    reqObjOneWay = {};
    cabinClass = 'ECONOMY';
    originDestinations = [];
    AirlineType;
    Adults = 0;
    Childs = 0;
    Infants = 0;
    navItems;
    origin = '';
    destination = '';
    userLoggedIn;
    fareInfoStatus;
    fareInfoData;
    todaysDate = new Date();
    returnDate: any;
    showMultiCity;
    airLineNameBasedOnCode;
    airPortNameBasedOnFromCode;
    airPortNameBasedOnToCode;
    selectedOption = 'oneWay';
    routeInfos;
    selectedPrice;
    departDate: any;
    multiObject = [];
    mergedSingleTripArr = [];
    totalFareRuleObj: any = {};
    value = 0;
    maxValue = 10000;
    minTime = 0;
    maxTime = 23;
    stop0 = false;
    stop1 = false;
    stop2 = false;
    stop3 = false;
    stop4 = false;
    options: Options = {
        floor: 0,
        ceil: 10000
    };
    timeOptions: Options = {
        floor: 0,
        ceil: 23
    };
    resultsArr: any = [];
    tempResultsArr: any = [];
    tempArr;
    tripinfos: any;
    showMulticity: boolean;
    roundTrip: boolean;
    notRoundTrip: boolean;
    specialTrip;
    topLeftData;
    leftdata;
    topRightData;
    mulCity: any[];
    cities = [{
        value: 'DEL',
        viewValue: 'Delhi'
    }, {
        value: 'BOM',
        viewValue: 'Mumbai'
    }, {
        value: 'BLR',
        viewValue: 'Bangalore'
    }];
    flightClass = [{
        value: 'ECONOMY',
        viewValue: 'Economy'
    }, {
        value: 'PREMIUM ECONOMY',
        viewValue: 'Premium Economy'
    }, {
        value: 'Business',
        viewValue: 'Business'
    }];
    tripJackMultiCity: any = [];
    tripJackMutliObj = {};
    onWardArr: any;
    returnArr: any;
    towardsTripJackObj;
    tripjackreturn;
    tripJackData;
    selectedItemsList = [];
    checkedIDs = [];
    currentlyDisabled = true;
    data;
    //selectedPrice;
    roundTripReturnSelectedPrice;
    resultArray: any[];
    constructor(private router: Router, private activeRoute: ActivatedRoute, private dialog: MatDialog, private helperService: HelperService,
        private flightResultsHelper: FlightresulthelperService, private flightSearchDetailsService: FlightsearchService,
        private spinner: NgxSpinnerService, private landingService: LandingService, private _bottomSheet: MatBottomSheet) { }

    ngOnInit() {
        this.navItems = this.helperService.getNavItems();
        this.originDestinations = this.helperService.originDestinations();
        if (this.activeRoute.snapshot.params.id === 'roundTrip') {
            this.currentlyDisabled = false;
            this.origin = localStorage.getItem('origin');
            this.destination = localStorage.getItem('destination');
            this.departDate = new Date(localStorage.getItem('departDate'));
            this.returnDate = new Date(localStorage.getItem('returnDate'));
            this.Adults = Number(localStorage.getItem('Adults'));
            this.Childs = Number(localStorage.getItem('Childs'));
            this.Infants = Number(localStorage.getItem('Infants'));
            this.cabinClass = localStorage.getItem('CabinClass');
            this.AirlineType = localStorage.getItem('AirlineType');
            this.showMulticity = false;
            this.roundTrip = true;
            this.notRoundTrip = false;
            this.specialTrip = false;
            this.towardsTripJackObj = JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos.ONWARD;
            console.log(this.towardsTripJackObj)
            this.tripjackreturn = JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos.RETURN;
            console.log(this.tripjackreturn)
            // this.totalFare=JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos.T;
            this.tripjackreturn.forEach(element => {
                const diffInHours = differenceInHours(parseISO(element.sI[0].at), parseISO(element.sI[0].dt));
                const diffInMinutes = differenceInMinutes(parseISO(element.sI[0].at), parseISO(element.sI[0].dt)) % 60;
                element.remainingTime = diffInHours + 'hrs ' + diffInMinutes + 'mins';

            });
            this.towardsTripJackObj.forEach(element => {
                const diffInHours = differenceInHours(parseISO(element.sI[0].at), parseISO(element.sI[0].dt));
                const diffInMinutes = differenceInMinutes(parseISO(element.sI[0].at), parseISO(element.sI[0].dt)) % 60;
                element.remainingTime = diffInHours + 'hrs ' + diffInMinutes + 'mins';

            });

        }
        else if (this.activeRoute.snapshot.params.id === 'multiCity') {

            let dat=localStorage.getItem('resultData')
            console.log(dat);
            // this.tempArr = JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos;
            // console.log(this.tempArr);

            if (JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos.COMBO) {
                this.tempArr = JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos.COMBO;
                console.log(this.tempArr);
            } else {

                this.tempArr = JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos;
                console.log(this.tempArr);
            }

            //this.tempArr = JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos;
            this.showMulticity = true;
            this.roundTrip = false;
            this.notRoundTrip = true;
            for (const obj in this.tempArr) {
                if (obj) {
                    this.tempArr[obj].forEach(element => {
                        this.resultsArr.push(element);
                        console.log(this.resultsArr)
                    });
                    this.resultsArr.forEach(element => {
                        const diffInHours = differenceInHours(parseISO(element.sI[0].at), parseISO(element.sI[0].dt));
                        const diffInMinutes = differenceInMinutes(parseISO(element.sI[0].at), parseISO(element.sI[0].dt)) % 60;
                        element.remainingTime = diffInHours + 'hrs ' + diffInMinutes + 'mins';

                    });
                }
                const headingArr = zip(JSON.parse(localStorage.getItem('multiFromAirportCodes')),
                    JSON.parse(localStorage.getItem('multiToAirportCodes')));
                    console.log(headingArr);
                const headingDates = JSON.parse(localStorage.getItem('multiDateArr'));
                console.log(headingDates);
            }

        }
        else {

            this.currentlyDisabled = true;
            this.origin = localStorage.getItem('origin');
            console.log(this.origin)
            this.destination = localStorage.getItem('destination');
            this.departDate = new Date(localStorage.getItem('departDate'));
            this.Adults = Number(localStorage.getItem('Adults'));
            this.Childs = Number(localStorage.getItem('Childs'));
            this.Infants = Number(localStorage.getItem('Infants'));
            this.cabinClass = localStorage.getItem('CabinClass');
            this.AirlineType = localStorage.getItem('AirlineType');

            this.showMulticity = false;
            this.specialTrip = false;
            this.notRoundTrip = true;
            this.resultsArr = JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos.ONWARD;

            this.resultsArr.forEach(element => {
                // let fare=localStorage.setItem("id",element.totalPriceList[0].id)

                const diffInHours = differenceInHours(parseISO(element.sI[0].at), parseISO(element.sI[0].dt));
                const diffInMinutes = differenceInMinutes(parseISO(element.sI[0].at), parseISO(element.sI[0].dt)) % 60;
                element.remainingTime = diffInHours + 'hrs ' + diffInMinutes + 'mins';


                console.log(element.remainingTime)


            });
            this.resultsArr.forEach(obj => {


                console.log(obj.totalPriceList[0].id);
                obj.fareId = obj.totalPriceList[0].id;
                console.log(obj.fareId)

            });

        }
        const tokenhelper = new JwtHelperService();
        const isExpired = tokenhelper.isTokenExpired(localStorage.getItem('token'));

        if (localStorage.getItem('token') && !isExpired) {


            this.userLoggedIn = true;
        } else {
            localStorage.removeItem('token');


            this.userLoggedIn = false;
        }
        this.fetchSelectedItems()
        this.fetchCheckedIDs()

        // }
        // this.stopCheck('zero', 'stop0');
    }

    changeSelection() {
        this.fetchSelectedItems()
    }

    fetchSelectedItems() {
        this.selectedItemsList = this.towardsTripJackObj.filter((value, index) => {
            return value.isChecked
        });
    }

    fetchCheckedIDs() {
        this.checkedIDs = []
        this.towardsTripJackObj.forEach((value, index) => {
            if (value.isChecked) {
                this.checkedIDs.push(value.id);
            }
        });
    }
    openSearchDialog() {
        const dialogRef = this.dialog.open(SearchcomponentComponent, {
            disableClose: true,
            panelClass: 'login-container'
        });
        dialogRef.afterClosed().subscribe(data => {
            console.log(data);
            if (data) {

            }
        });
    }
    onPriceSelectedChange(e) {
        if (typeof e.value === 'string' || e.value instanceof String) {
            this.selectedPrice = Number(e.value);
        } else {
            this.selectedPrice = e.value;
        }
    }

    //else {
    //this.fareInfoStatus = true;
    //}

    onFlightSelect(obj) {
        obj.selPrice = this.selectedPrice;
        // this.topLeftData = obj;

        // localStorage.setItem('leftData', obj);
        // let far=localStorage.getItem('fareId')
        //console.log(far)
        let tol = localStorage.getItem('token');
        console.log(tol);
        // if (localStorage.getItem('token')) {
        if (this.activeRoute.snapshot.params.id !== 'roundTrip') {
            const sendObject = {
                priceIds: [obj.fareId]
            };
            console.log(sendObject);
            this.spinner.show();
            this.flightSearchDetailsService.flightReview(sendObject).subscribe(data => {
                this.spinner.hide();
                console.log(data)
                let sing = localStorage.setItem("singleBookFlightData", JSON.stringify(data));
                let singlebook = localStorage.getItem('singleBookFlightData')
                console.log(singlebook)

                this.router.navigateByUrl('flight-review');
            });
        }
        //}

        // else {
        //     console.log("came into userlogged in condition")
        //     this.router.navigateByUrl('flight-review');


        // }
        //  else {
        //     this.dialog.open(LoginComponent, {
        //         disableClose: true,
        //         panelClass: 'dialog-container'
        //       });
        //       this.router.navigateByUrl('search-results');
        //       if(localStorage.getItem('token')){
        //         this.router.navigateByUrl('flight-review');
        //       }
        // }


    }
    onResultMatTabSelected(e, fareId) {
        if (e.index === 1 && fareId) {
            const obj = {
                id: fareId,
                flowType: 'REVIEW'
            };
            this.spinner.show();
            this.flightSearchDetailsService.fareRules(obj).subscribe(data => {
                this.spinner.hide();
                console.log(data);
                if (data.fareRule[localStorage.getItem('origin') + '-' + localStorage.getItem('destination')]) {
                    this.totalFareRuleObj = data.fareRule[localStorage.getItem('origin') + '-' + localStorage.getItem('destination')];
                }
                this.fareInfoData = true;
            });
        } else {
            this.fareInfoStatus = true;
        }
    }
    onLeftFlightSelect(leftobj) {
        localStorage.setItem('singleFlightData', JSON.stringify(leftobj));
        localStorage.setItem('LeftFareId', leftobj.totalPriceList[0].id)
        let fareid = localStorage.getItem('LeftFareId')
        console.log(fareid);
        this.topLeftData = leftobj;
    }

    onRightFlightSelect(rightobj) {
        // rightobj.selPrice = this.roundTripReturnSelectedPrice;
        localStorage.setItem('singleReturnBookFlightData', JSON.stringify(rightobj));
        localStorage.setItem('rightFared', rightobj.totalPriceList[0].id)
        let rightfareid = localStorage.getItem('rightFared')
        console.log(rightfareid);
        this.topRightData = rightobj;
        this._bottomSheet.open(BottonSheetComponent, {
            backdropClass: 'customBackDrop',
            data: {
                leftData: this.topLeftData,
                rightData: rightobj,
            }
        });
        // if (localStorage.getItem('userSessionToken')) {
        // //   this.router.navigateByUrl('results-view');
        // } else {
        //     this.dialog.open(LoginComponent, {
        //         disableClose: true,
        //         panelClass: 'dialog-container'
        //       });
        // }
    }

    onUserChangeEnd(e) {
        this.resultsArr = JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos.ONWARD;
        this.resultsArr = this.resultsArr.filter((obj: any) =>
            obj.totalPriceList[0].fd.ADULT.fC.TF >= e.value &&
            obj.totalPriceList[0].fd.ADULT.fC.TF <= e.highValue
        );
    }

    onTimeChangeEnd(e) {
        this.resultsArr = JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos.ONWARD;
        this.resultsArr = this.resultsArr.filter((obj: any) =>
            parseISO(obj.sI[0].dt).getHours() >= e.value &&
            parseISO(obj.sI[0].dt).getHours() <= e.highValue
        );
    }

    stopCheck(item, value) {
        switch (item) {
            case 'zero':
                if (value) {
                    this.resultsArr = JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos.ONWARD;
                    this.resultsArr = this.resultsArr.filter((obj: any) => {
                        if (obj.sI[0].so) {
                            return obj.sI[0].so.length !== 0 || obj.sI[0].stops !== 0;
                        }
                    });
                }
                break;
            case 'one':
                if (value) {
                    this.resultsArr = JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos.ONWARD;
                    this.resultsArr = this.resultsArr.filter((obj: any) =>
                        obj.sI[0].stops === 1
                    );
                } else {
                    this.resultsArr = JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos.ONWARD;
                }
                break;
            case 'two':
                if (value) {
                    this.resultsArr = JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos.ONWARD;
                    this.resultsArr = this.resultsArr.filter((obj: any) =>
                        obj.sI[0].stops === 2
                    );
                } else {
                    this.resultsArr = JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos.ONWARD;
                }
                break;
            case 'three':
                if (value) {
                    this.resultsArr = JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos.ONWARD;
                    this.resultsArr = this.resultsArr.filter((obj: any) =>
                        obj.sI[0].stops === 3
                    );
                } else {
                    this.resultsArr = JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos.ONWARD;
                }
                break;
            default:
                if (value) {
                    this.resultsArr = JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos.ONWARD;
                    this.resultsArr = this.resultsArr.filter((obj: any) =>
                        obj.sI[0].stops > 3
                    );
                } else {
                    this.resultsArr = JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos.ONWARD;
                }
        }
    }

    openRoundSearchDialog() {
        this.dialog.open(RoundDialogComponent, {
            disableClose: false,

        });

    }
    onOriginChange(value) {
        sessionStorage.setItem('prevOrigin', value);
    }

    onDestinationChange(value) {
        sessionStorage.setItem('prevDestination', value);
    }

    onTodaysDate() {
        if (this.departDate) {
            this.returnDate = new Date(this.departDate);
        }
    }

    onReturnDate() {

    }

    changeChipSelected(value) {
        this.selectedOption = value;
        switch (value) {
            case 'oneWay':
                this.showReturnFlightDate = false;
                this.showMultiCity = false;
                break;
            case 'multiCity':
                this.showMultiCity = true;
                break;
            default:
                this.showReturnFlightDate = true;
                this.showMultiCity = false;
        }
    }

    onFlightSearch() {
        if (this.departDate) {
            this.departDate = format(new Date(this.departDate), 'yyyy-MM-dd');
        }
        // if (this.multiObject[0].travelDate) {
        //   this.multiObject.forEach(obj => {
        //     obj.fromCityOrAirport = {
        //       code : obj.fromCityOrAirport
        //     };
        //     obj.toCityOrAirport = {
        //       code : obj.toCityOrAirport
        //     };
        //     obj.travelDate = format(new Date(obj.travelDate), 'yyyy-MM-dd');
        //   });
        // }
        if (this.selectedOption === 'multiCity') {
            this.routeInfos = this.multiObject;
        } else {
            this.routeInfos = [{
                fromCityOrAirport: {
                    code: this.origin
                },
                toCityOrAirport: {
                    code: this.destination
                },
                travelDate: this.departDate
            }];
        }
        this.reqObjOneWay = {
            searchQuery: {
                cabinClass: this.cabinClass,
                paxInfo: {
                    ADULT: this.Adults,
                    CHILD: this.Childs,
                    INFANT: this.Infants
                },
                routeInfos: this.routeInfos,
                searchModifiers: {
                    isDirectFlight: true,
                    isConnectingFlight: true
                }
            }
        };
        this.spinner.show();
        this.landingService.searchFlights(this.reqObjOneWay).subscribe(data => {
            if (data) {
                this.spinner.hide();
                if (this.activeRoute.snapshot.params.id === 'roundTrip') {
                    this.showMulticity = false;
                    this.roundTrip = true;
                    this.notRoundTrip = false;
                    this.resultsArr = JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos.ONWARD;
                    this.resultsArr.forEach(element => {
                        const diffInHours = differenceInHours(parseISO(element.sI[0].at), parseISO(element.sI[0].dt));
                        const diffInMinutes = differenceInMinutes(parseISO(element.sI[0].at), parseISO(element.sI[0].dt)) % 60;
                        element.remainingTime = diffInHours + 'hrs ' + diffInMinutes + 'mins';
                    });
                } else if (this.activeRoute.snapshot.params.id === 'multiCity') {
                    this.tempArr = JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos;
                    this.showMulticity = true;
                    this.roundTrip = false;
                    this.notRoundTrip = true;
                    for (const obj in this.tempArr) {
                        if (obj) {
                            this.tempArr[obj].forEach(element => {
                                this.resultsArr.push(element);
                            });
                        }
                    }
                } else {
                    // console.log("elae block")
                    this.showMulticity = false;
                    // this.roundTrip = false;
                    this.notRoundTrip = true;
                    this.resultsArr = JSON.parse(localStorage.getItem('resultData')).searchResult.tripInfos.ONWARD;
                    this.resultsArr.forEach(element => {

                        const diffInHours = differenceInHours(parseISO(element.sI[0].at), parseISO(element.sI[0].dt));
                        const diffInMinutes = differenceInMinutes(parseISO(element.sI[0].at), parseISO(element.sI[0].dt)) % 60;
                        element.remainingTime = diffInHours + 'hrs ' + diffInMinutes + 'mins';
                        console.log("cursor came to here")
                    });
                }
            } else {
                this.spinner.hide();
            }
        });
    }
    // openBottomSheet(leftobj) {
    //     this.leftdata=leftobj;
    //     this._bottomSheet.open(BottonSheetComponent, {

    //         backdropClass: 'customBackDrop',
    //         data:this.leftdata

    //     });
    //   }
}
