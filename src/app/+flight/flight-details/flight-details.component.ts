import { Component, OnInit,ViewChild } from '@angular/core';

import { FormGroup, FormControl,FormBuilder, Validators ,FormArray} from '@angular/forms';

import { LoginComponent } from 'src/app/login/login.component';
import { JwtHelperService } from '@auth0/angular-jwt';
import { MatDialog } from '@angular/material/dialog';


import { parseISO, differenceInHours, differenceInMinutes, getHours } from 'date-fns';

import { Router, ActivatedRoute } from '@angular/router';
import {MatAccordion} from '@angular/material/expansion';
@Component({
  selector: 'app-flight-details',
  templateUrl: './flight-details.component.html',
  styleUrls: ['./flight-details.component.scss']
})
export class FlightDetailsComponent implements OnInit {
  resultsObj: any = {};
  resultsArr = [];
  adultinfo;
  infantinfo;
  childinfo;
  parsedAarivalDate;
  parsedDepartureDate;
  username;
  userLoggedIn;
  personsForm:FormGroup;
  childForm:FormGroup;
  InfantDetailsForm:FormGroup;
  rightreviewData;
  showFields;
  reviewData;
  fromAirportCode;
  toAirportCode;
  adultCount;
  childCount;
  infantCount;
  airlineName;
  airlineCode;
  flightNo;
  flightClass;
  fromAirportName;
  toAirportName;
  depTime;
  arrTime;
  depDate;
  arrDate;
  fromTerminal;
  toTerminal;
  adultFare;
  childFare;
  infantFare;
  fuelCharges;
  cgst;
  sgst;
  cuteFee;
  totalAmount;
  remainingTime;
  rightdiffInHours;
  rightdiffInMinutes;
  rightremainingTime;
  rightfromAirportCode;
  righttoAirportCode;
  rightairlineName;
  rightairlineCode;
  rightflightNo;
  rightflightClass;
  rightfromAirportName;
  righttoAirportName;
  rightdepTime;
  rightarrTime;
  rightdepDate;
  rightarrDate;
  rightfromTerminal;
  righttoTerminal;
  righttotalAmount;
  basefare;
  netfare;
  taffare;
  constructor(private router: Router, private activeRoute: ActivatedRoute,private fb: FormBuilder,private dialog: MatDialog) { }
  @ViewChild(MatAccordion) accordion: MatAccordion;
  ngOnInit() {
    // if (localStorage.getItem('flightDataType') === 'tripJack') {
      this.showFields = true;
      this.reviewData = JSON.parse(localStorage.getItem('singleBookFlightData'));
      console.log(this.reviewData);
      this.fromAirportCode = this.reviewData.searchQuery.routeInfos[0].fromCityOrAirport.code;
      this.toAirportCode = this.reviewData.searchQuery.routeInfos[0].toCityOrAirport.code;
      this.adultCount = this.reviewData.searchQuery.paxInfo.ADULT;
      this.childCount = this.reviewData.searchQuery.paxInfo.CHILD;
      this.infantCount = this.reviewData.searchQuery.paxInfo.INFANT;
      this.airlineName = this.reviewData.tripInfos[0].sI[0].fD.aI.name;
      this.airlineCode = this.reviewData.tripInfos[0].sI[0].fD.aI.code;
      this.flightNo = this.reviewData.tripInfos[0].sI[0].fD.fN;
      this.flightClass = this.reviewData.searchQuery.cabinClass;
      this.fromAirportName = this.reviewData.searchQuery.routeInfos[0].fromCityOrAirport.name;
      this.toAirportName = this.reviewData.searchQuery.routeInfos[0].toCityOrAirport.name;
      this.depTime = this.reviewData.tripInfos[0].sI[0].dt.split('T')[1];
      this.arrTime = this.reviewData.tripInfos[0].sI[0].at.split('T')[1];
      this.depDate = this.reviewData.tripInfos[0].sI[0].dt.split('T')[0];
      this.arrDate = this.reviewData.tripInfos[0].sI[0].at.split('T')[0];
      this.fromTerminal = this.reviewData.tripInfos[0].sI[0].da.terminal;
      this.toTerminal = this.reviewData.tripInfos[0].sI[0].aa.terminal;
      this.totalAmount = this.reviewData.totalPriceInfo.totalFareDetail.fC.TF;
      this.basefare= this.reviewData.totalPriceInfo.totalFareDetail.fC.BF;
      this.netfare= this.reviewData.totalPriceInfo.totalFareDetail.fC.NF;
      this.taffare= this.reviewData.totalPriceInfo.totalFareDetail.fC.TAF;
      const diffInHours = differenceInHours(parseISO(this.reviewData.tripInfos[1].sI[0].at), parseISO(this.reviewData.tripInfos[1].sI[0].dt));
      const diffInMinutes = differenceInMinutes(parseISO(this.reviewData.tripInfos[1].sI[0].at), parseISO(this.reviewData.tripInfos[1].sI[0].dt)) % 60;
      this.remainingTime = diffInHours + 'hrs ' + diffInMinutes + 'mins';
      console.log(this.remainingTime)

      // this.rightreviewData = JSON.parse(localStorage.getItem('singleBookFlightData'));
      // console.log(this.rightreviewData);
      this.rightfromAirportCode = this.reviewData.searchQuery.routeInfos[1].fromCityOrAirport.code;
      this.righttoAirportCode = this.reviewData.searchQuery.routeInfos[1].toCityOrAirport.code;
      // this.adultCount = this.reviewData.searchQuery.paxInfo.ADULT;
      // this.childCount = this.reviewData.searchQuery.paxInfo.CHILD;
      // this.infantCount = this.rightreviewData.searchQuery.paxInfo.INFANT;
      // this.rightairlineName = this.reviewData.tripInfos[1].sI[0].fD.aI.name;
      // this.rightairlineCode = this.reviewData.tripInfos[1].sI[0].fD.aI.code;
      // this.rightflightNo = this.reviewData.tripInfos[1].sI[0].fD.fN;
      // this.rightflightClass = this.reviewData.searchQuery.cabinClass;
      // this.rightfromAirportName = this.reviewData.searchQuery.routeInfos[0].fromCityOrAirport.name;
      // this.righttoAirportName = this.reviewData.searchQuery.routeInfos[0].toCityOrAirport.name;
      // this.rightdepTime = this.reviewData.tripInfos[1].sI[0].dt.split('T')[1];
      // this.rightarrTime = this.reviewData.tripInfos[1].sI[0].at.split('T')[1];
      // this.rightdepDate = this.reviewData.tripInfos[1].sI[0].dt.split('T')[0];
      // this.rightarrDate = this.reviewData.tripInfos[1].sI[0].at.split('T')[0];
      // this.rightfromTerminal = this.reviewData.tripInfos[1].sI[0].da.terminal;
      // this.righttoTerminal = this.reviewData.tripInfos[0].sI[0].aa.terminal;
      // this.righttotalAmount = this.reviewData.totalPriceInfo.totalFareDetail.fC.TF;
      

      // const rightdiffInHours = differenceInHours(parseISO(this.reviewData.tripInfos[1].sI[0].at), parseISO(this.reviewData.tripInfos[1].sI[0].dt));
      // const rightdiffInMinutes = differenceInMinutes(parseISO(this.reviewData.tripInfos[1].sI[0].at), parseISO(this.reviewData.tripInfos[1].sI[0].dt)) % 60;
      // this.rightremainingTime = rightdiffInHours + 'hrs ' + rightdiffInMinutes + 'mins';
      // console.log(this.rightremainingTime)

      if (localStorage.getItem('singleReturnBookFlightData') === null) {
        this.resultsObj = JSON.parse(localStorage.getItem('singleBookFlightData'));
        this.parsedDepartureDate = parseISO(this.resultsObj.tripInfos[0].sI[0].dt);
        console.log(this.parsedDepartureDate);
        this.parsedAarivalDate = parseISO(this.resultsObj.tripInfos[0].sI[0].at);
      } else {
        this.resultsObj = JSON.parse(localStorage.getItem('singleBookFlightData'));
        console.log(this.resultsObj);
      }
       
      this.personsForm = this.fb.group({
        persons:this.fb.array([]),
        children: this.fb.array([]),
        infants:this.fb.array([]),
      });
     
      

      
    }

    onFormSubmit() {
      alert(JSON.stringify(this.personsForm.value));
    }
    createPersonFormGroup() {
      return this.fb.group({
        firstname: ['', [Validators.required]],
        lastname: ['', [Validators.required]],
    
      })
    }
    createChildFormGroup() {
      return this.fb.group({
        firstname: ['', [Validators.required]],
        lastname: ['', [Validators.required]],
    
      })
    }
    createInfantFormGroup() {
      return this.fb.group({
        firstname: ['', [Validators.required]],
        lastname: ['', [Validators.required]],
    
      })
    }
  
  
  
    
  
    addPerson() {
      (<FormArray>this.personsForm.get('persons')).push(this.fb.group({
        adultfirstname:[],
        adultlastname:[]
      }));
    }
  
    get persons() {
      return (<FormArray>this.personsForm.get('persons')).controls;
    }
  
    removePerson(i) {
      (<FormArray>this.personsForm.get('persons')).removeAt(i);
    }
  
  
  
    addChild() {
      (<FormArray>this.personsForm.get('children')).push(this.fb.group({
        childfirstname:[],
        childlastname:[]
      }));
    }
  
    get children() {
      return (<FormArray>this.personsForm.get('children')).controls;
    }
  
    removechild(i) {
      (<FormArray>this.personsForm.get('children')).removeAt(i);
    }
    
    addinfant() {
      (<FormArray>this.personsForm.get('infants')).push(this.fb.group({
        infantfirstname:[],
        infantlastname:[]
      }));
    }
  
    get infants() {
      return (<FormArray>this.personsForm.get('infants')).controls;
    }
  
    removeinfant(i) {
      (<FormArray>this.personsForm.get('infants')).removeAt(i);
    }
  onEditBooking() {
    this.router.navigate(['search-results', localStorage.getItem('selectedSearchOption')]);
  }
  confirmBooking() {
         let tok=localStorage.getItem('token')
         if(tok===null){
          const dialogRef = this.dialog.open(LoginComponent, {
            disableClose: true,
            panelClass: 'login-container'
          });
          dialogRef.afterClosed().subscribe(data => {
            console.log(data);
            if (data) {
              // this.userLoggedIn = true;
              // this.userDetails = data;
              // localStorage.setItem('userExists', JSON.stringify(data));
              //this.router.navigateByUrl('flight-details');
            }
          });
         }
  
      }
}