import { Component, OnInit ,ViewChild} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';
import { SignupService } from 'src/app/signup/signup.service';

import { ToastrService } from 'ngx-toastr';
import {CommonService} from 'src/app/common/common.service';
import * as CryptoJS from 'crypto-js';
import { Constants } from 'src/app/common/constants.enum';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {MatAccordion} from '@angular/material/expansion';
import { VerifyComponent } from 'src/app/verify/verify.component';


@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.component.html',
  styleUrls: ['./myprofile.component.scss'],
  providers: [SignupService]
})
export class MyprofileComponent implements OnInit {

  @ViewChild(MatAccordion) accordion: MatAccordion;
  MyProfileForm: FormGroup;
  adminStatus;
  
  
  constructor(private router: Router, 
              private dialog: MatDialog,
              private formBuilder: FormBuilder, 
              private activeRoute: ActivatedRoute,
              private SignupService:SignupService
              ) { }

  
      ngOnInit() {
        this.MyProfileForm = this.formBuilder.group({
          name:['',Validators.required],
          
          email:['',Validators.required],
          phone:['',Validators.required],
           
        });
        let userid = localStorage.getItem('userId');
        // let contact=localStorage.getItem("phone",);
        // let email=localStorage.getItem("email",);

        console.log(userid)
        //let obj = {id : userId}
        //console.log(obj.id)
        console.log("came to My profile")

        this.SignupService.GettingUSer(userid).subscribe(data => {
          
          if (data) {
            console.log(data)
            //console.log("printted the data")
            this.MyProfileForm.patchValue({
              name: data.name,
             
              email: data.email,
              phone:data.phone

        
              
              
            });
          }
        });
        
    
    
        
    
    
          
         

    
    }
    // openEmailDialog() {
    //   const dialogRef = this.dialog.open(VerifyComponent, {
    //     disableClose: true,
    //     panelClass: 'login-container'
    //   });


    openEmailDialog() {
      const dialogRef = this.dialog.open(VerifyComponent, {
        disableClose: true,
        panelClass: 'login-container'
      });
      dialogRef.afterClosed().subscribe(data => {
        console.log(data);
        if (data) {
          // this.userLoggedIn = true;
          // this.userDetails = data;
          // localStorage.setItem('userExists', JSON.stringify(data));
        }
      });
    }
}
