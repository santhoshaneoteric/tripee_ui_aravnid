import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Constants } from '../common/constants.enum';

@Injectable({
  providedIn: 'root'
})
export class SignupService {
  constructor(private http: HttpClient) { }
  create(obj: object): Observable<any> {
    return this.http.post(Constants.API_URL + 'v1/auth/sign_up', obj, {})
      .pipe(map((res: any) => res));
  }
  // login(obj: object): Observable<any>  {
  //   return this.http.post(Constants.API_URL + 'login', obj, {})
  //     .pipe(map((res: any) => res));
  // }
  changePassword(id: string, obj: object): Observable<any>  {
    return this.http.post(Constants.API_URL + 'change_password/' + id, obj, {})
      .pipe(map((res: any) => res));
  }
  GettingUSer(id: string): Observable<any>  {
    return this.http.get(Constants.API_URL + 'v1/auth/user/' + id, {})
      .pipe(map((res: any) => res));
  }
}
