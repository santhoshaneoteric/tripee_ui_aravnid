import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { LoginComponent } from '../login/login.component';
import { SignupService } from './signup.service';
import { Router, NavigationEnd } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
  providers: [SignupService]
})
export class SignupComponent implements OnInit {
  roles = ['Admin', 'Customer', 'Operations', 'Front Office',
  'PackageAgent', 'PackageSubAgent', 'Manager',
  'BlogOperator', 'VisaOperator', 'PackageOperator', 'DestinationOperator',
  'ActivitiesOperator', 'CarOperator', 'Accounting', 'Sales', 'CarVendor',
  'CarDriver', 'User'];
  signupForm: FormGroup;
  constructor(private dialog: MatDialog, private dialogRef: MatDialogRef<SignupComponent>,
              private fb: FormBuilder, private signupService: SignupService,private router:Router,private toastr:ToastrService) { }

  ngOnInit() {
    this.signupForm = this.fb.group({
      name: ['', Validators.required],
      phone: ['',  [Validators.required, Validators.maxLength(10)]],
      email: ['', [
        Validators.required,
        Validators.pattern('^[A-Za-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
      ]],
      password:['', [Validators.required, Validators.minLength(6)]],
      role: ['user'],
      status: ['pending']
    });
  }

  openLoginDialog() {
    this.dialog.open(LoginComponent, {
        disableClose: true,
        panelClass: 'login-container'
    });
  }

  signupDialogClose() {
    this.dialog.closeAll();
  }

  onCreateAccount() {
    console.log(this.signupForm.value);
    this.signupService.create(this.signupForm.value).subscribe(data => {
      this.dialog.closeAll();
      if (data) {
        console.log(data)

        //console.log(data.email)
        localStorage.setItem('email', data.email);
       // this.commonService.saveUserInfo(data.data);

        this.dialogRef.close(data);
        this.dialog.open(LoginComponent, {
          disableClose: true,
          panelClass: 'login-container'
      });
    



      }
      else{
       // localStorage.setItem('error',data.email)
        this.toastr.warning( 'Email Must be Unique');

      }
    });
  }

}
